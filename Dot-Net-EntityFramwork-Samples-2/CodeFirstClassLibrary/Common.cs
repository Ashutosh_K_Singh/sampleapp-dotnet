﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Data.Entity;
using System.Globalization;

namespace CodeFirstClassLibrary
{
    public class Common
    {
        /// <summary>
        /// Get all properties type specifed in DB Set
        /// </summary>
        /// <param name="contextType"></param>
        /// <returns>Array of types</returns>
        public static Type[] GetGenericTypesOfDbSet(Type contextType)
        {
            var dbsetTypes = contextType.GetProperties().Where(f => f.PropertyType.IsGenericType == true && f.PropertyType.IsInterface == false && f.PropertyType.Name.Contains("DbSet`1")).Select(s => s.PropertyType.GetGenericArguments()[0]).ToArray();

            return dbsetTypes;
        }
        public static List<PropertyInfo> MangeCollectionProperties(PropertyInfo[] properties)
        {
            List<PropertyInfo> noneCollectionProperties = new List<PropertyInfo>();
            foreach (var property in properties)
            {
                if (!IsICollectionProperties(property.PropertyType))
                {
                    noneCollectionProperties.Add(property);
                }
            }

            return noneCollectionProperties;
        }

        public static List<PropertyInfo> ManageCollectionProperties(PropertyInfo[] propertyInfo, bool include)
        {
            List<PropertyInfo> properties = new List<PropertyInfo>();

            foreach (var property in propertyInfo)
            {
                bool isCollectionProperty = IsICollectionProperties(property.PropertyType);

                if (isCollectionProperty && include)
                {
                    properties.Add(property);
                }
                else if (!isCollectionProperty && !include)
                {
                    properties.Add(property);
                }
            }

            return properties;
        }

        public static List<PropertyInfo> GetPropertiesByDeclaration(List<PropertyInfo> properties, DeclaredType declaredType)
        {
            List<PropertyInfo> complexTypeProperties = new List<PropertyInfo>();
            foreach (var property in properties)
            {
                var iscomplex = IsPropertyComplexType(property.PropertyType);

                if (iscomplex && DeclaredType.Complex == declaredType)
                {
                    complexTypeProperties.Add(property);
                }
                else if (!iscomplex && DeclaredType.Simple == declaredType)
                {
                    complexTypeProperties.Add(property);
                }
            }

            return complexTypeProperties;
        }

        public static Dictionary<string, string> BuildPropertyMappingDictionary(List<PropertyInfo> simpleProperties, List<PropertyInfo> complexProperties)
        {
            Dictionary<string, string> propertyDictionary = new Dictionary<string, string>();

            Dictionary<string, string> complexPropertiesDictionary = new Dictionary<string, string>();


            /// Logic for simple type properties
            foreach (var property in simpleProperties)
            {
                FindPropertyAttribute(propertyDictionary, property);
            }
           
            BuildComplexProperty(complexProperties, propertyDictionary);

            
            return propertyDictionary;
        }

        private static void BuildComplexProperty(List<PropertyInfo> complexProperties, Dictionary<string, string> propertyDictionary)
        {
            foreach (var ctp in complexProperties)
            {
                string propertyTypeName = ctp.Name;
                Dictionary<string, string> complexDictionary = new Dictionary<string, string>();

                bool isVirtual = ctp.GetGetMethod().IsVirtual;

                var innerproperties = MangeCollectionProperties(ctp.PropertyType.GetProperties()).ToArray();

                PropertyInfo pinfo = KeyAttributeAvailable(innerproperties, out bool KeyAvailable);

                if (KeyAvailable)
                {
                    FindPropertyAttributForComplexTypeDrived(complexDictionary, pinfo, ctp.Name);
                }
                else
                {
                    foreach (var p in innerproperties)
                    {
                        FindPropertyAttributForComplexTypeDrived(complexDictionary, p, ctp.Name, isVirtual);
                    }
                }

                if (isVirtual)
                {
                    if (propertyDictionary.Where(k => complexDictionary.ContainsKey(k.Key)).Count() == 0)
                    {
                        foreach (var cd in complexDictionary)
                        {
                            bool c = new string[] { "Id", propertyTypeName + "Id" }.Contains(cd.Key);

                            if (!propertyDictionary.ContainsKey(cd.Key) && c)
                            {
                                string underScorePropertyname = propertyTypeName + "_" + cd.Value;
                                propertyDictionary.Add(cd.Key, underScorePropertyname);
                            }
                        }
                    }
                }
                else
                {
                    foreach (var cd in complexDictionary)
                    {
                        if (!propertyDictionary.ContainsKey(cd.Key))
                        {
                            propertyDictionary.Add(cd.Key, cd.Value);
                        }
                    }
                }
            }
        }

        private static bool IsICollectionProperties(Type type)
        {
            if (type.UnderlyingSystemType != typeof(string))
            {
                if (type.GetInterface(nameof(System.Collections.IList)) != null || type.GetInterface(nameof(System.Collections.IEnumerable)) != null || type.GetInterface(nameof(System.Collections.ICollection)) != null)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Get complex type property mapping used in main entity
        /// </summary>
        /// <param name="propertyDictionary"></param>
        /// <param name="property"></param>
        /// <param name="propName"></param>
        public static void FindPropertyAttributForComplexTypeDrived(Dictionary<string, string> propertyDictionary, PropertyInfo property, string propName, bool isVirtual = false)
        {
            var propertyname = property.Name;

            string keypropertyName = property.Name;

            string columnName = propName + "_" + keypropertyName;

            string[] attributeTypes = property.GetCustomAttributes(true).Select(x => x.GetType().Name).ToArray();

            if (Array.FindAll(attributeTypes, a => a.Equals("NotMappedAttribute")).Count() == 0)
            {
                if (Array.FindAll(attributeTypes, a => a.Equals("KeyAttribute")).Count() > 0)
                {
                    FindPropertyAttribute(propertyDictionary, property);
                }
                else if (property.Name.ToUpper() == "ID" && property.PropertyType == typeof(int))
                {
                    FindPropertyAttribute(propertyDictionary, property);
                }
                else if (Array.FindAll(attributeTypes, a => a.Equals("KeyAttribute")).Count() == 0 && Array.FindAll(attributeTypes, a => a.Equals("ColumnAttribute")).Count() > 0)
                {
                    FindPropertyAttribute(propertyDictionary, property);
                }
                else if (isVirtual)
                {
                    propertyDictionary.Add(propertyname, propertyname);
                }
                else
                {
                    var propertyUnderScorName = propName + "_" + propertyname;
                    propertyDictionary.Add(propertyUnderScorName, propertyname);
                }

            }
        }

        /// <summary>
        /// Get attributes defined on entity's properties
        /// </summary>
        /// <param name="propertyDictionary"></param>
        /// <param name="property"></param>
        public static void FindPropertyAttribute(Dictionary<string, string> propertyDictionary, PropertyInfo property)
        {
            var propertyname = property.Name;

            string[] attributeTypes = property.GetCustomAttributes(true).Select(x => x.GetType().Name).ToArray();

            if (Array.FindAll(attributeTypes, a => a.Equals("NotMappedAttribute")).Count() == 0)
            {
                if (Array.FindAll(attributeTypes, a => a.Equals("ColumnAttribute")).Count() > 0)
                {
                    var columnattribetype = property.GetCustomAttributes(true).Where(x => x.GetType().Name == "ColumnAttribute").ToList();

                    string keyColumn = DynamicPropertyGetter(columnattribetype, "Name");

                    DuplicateKeyCheck(propertyDictionary, keyColumn, keyColumn, 0, propertyname);
                }
                else
                {
                    propertyDictionary.Add(propertyname, propertyname);
                }
            }
        }
        /// <summary>
        /// Check and update duplicate column attribute of an entity
        /// </summary>
        /// <param name="propertyDictionary"></param>
        /// <param name="key"></param>
        /// <param name="newKey"></param>
        /// <param name="idx"></param>
        /// <param name="propertyname"></param>
        /// <returns>true if duplicate column name exists</returns>      

        /// <summary>
        /// Get key attribute defined on entity's properties
        /// </summary>
        /// <param name="propertyinfo"></param>
        /// <param name="keyavailable"></param>
        /// <returns>Property</returns>
        public static PropertyInfo KeyAttributeAvailable(PropertyInfo[] propertyinfo, out bool keyavailable)
        {
            PropertyInfo keyPropertyInfo = null;
            keyavailable = true;
            foreach (var property in propertyinfo)
            {
                string[] attributeTypes = property.GetCustomAttributes(true).Select(x => x.GetType().Name).ToArray();

                if (Array.FindAll(attributeTypes, a => a.Equals("NotMappedAttribute")).Count() == 0)
                {
                    if (Array.FindAll(attributeTypes, a => a.Equals("KeyAttribute")).Count() > 0)
                    {
                        keyPropertyInfo = property;

                        break;
                    }
                    else if (property.Name.ToUpper() == "ID" && property.PropertyType == typeof(int))
                    {
                        keyPropertyInfo = property;
                    }
                    else
                    {
                        keyavailable = false;
                    }
                }
            }

            return keyPropertyInfo;
        }
        public static bool DuplicateKeyCheck(Dictionary<string, string> propertyDictionary, string key, string newKey, int idx, string propertyname)
        {
            if (propertyDictionary.ContainsKey(newKey))
            {
                var counter = idx > 0 ? idx.ToString() : string.Empty;

                var newkey = key + counter;

                idx++;

                DuplicateKeyCheck(propertyDictionary, key, newkey, idx, propertyname);
            }
            else
            {
                propertyDictionary.Add(newKey, propertyname);
            }

            return false;
        }
        /// <summary>
        /// Get data attribute notaion properties info
        /// </summary>
        /// <param name="columnattribetype"></param>
        /// <param name="propertyname"></param>
        /// <returns></returns>
        public static dynamic DynamicPropertyGetter(List<object> columnattribetype, string propertyname)
        {
            return columnattribetype[0].GetType().GetProperty(propertyname).GetValue(columnattribetype[0], null);
        }

        public static bool IsPropertyComplexType(Type propertyType)
        {
            bool iscomplextype = false;

            Type retval = propertyType.UnderlyingSystemType;

            Type nullableType = Nullable.GetUnderlyingType(retval);

            if (propertyType == typeof(Guid))
            {
                return false;
            }
            if (nullableType == null)
            {
                if (System.Type.GetTypeCode(propertyType) == TypeCode.Object)
                {
                    iscomplextype = true;
                }
            }
            else if (System.Type.GetTypeCode(nullableType) == TypeCode.Object)
            {
                iscomplextype = true;
            }

            return iscomplextype;
        }

        public static void FindPropertyAttributForComplexType(Dictionary<string, string> propertyDictionary, PropertyInfo property, string propertyName, bool IsVirtual = false)
        {
            var propertyname = property.Name;

            string[] attributeTypes = property.GetCustomAttributes(true).Select(x => x.GetType().Name).ToArray();

            if (Array.FindAll(attributeTypes, a => a.Equals("ColumnAttribute")).Count() > 0)
            {
                FindPropertyAttribute(propertyDictionary, property);
            }
            else if (IsVirtual)
            {
                propertyDictionary.Add(propertyname, propertyname);
            }
            else
            {
                var propertyUnderScorName = propertyName + "_" + propertyname;
                propertyDictionary.Add(propertyUnderScorName, propertyname);
            }
        }

        /// <summary>
        /// Get connection string name specified in conext class constructor which drived from Db context
        /// </summary>
        /// <param name="type"></param>
        /// <returns>connection string name</returns>
        public static string GeConnectionStringNameFromDbContextCunstructor(Type type)
        {
            var constructor = type.GetConstructors().FirstOrDefault(c => c.GetParameters().All(p => p.IsOptional));

            var parameters = constructor.GetParameters().Select(p => Type.Missing).ToArray();

            var instance = constructor.Invoke(BindingFlags.OptionalParamBinding | BindingFlags.InvokeMethod | BindingFlags.CreateInstance | BindingFlags.Instance, null, parameters, CultureInfo.InvariantCulture);

            var internalcontextprop = instance.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(p => p.Name == "InternalContext");

            var internalcontextpropvalue = internalcontextprop.GetValue(instance, null);

            var internalConnectionProp = internalcontextpropvalue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_internalConnection");

            var internalConnectionPropValue = internalConnectionProp.GetValue(internalcontextpropvalue);

            var nameOrConnectionStringProp = internalConnectionPropValue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_nameOrConnectionString");

            var nameOrConnectionStringPropValue = nameOrConnectionStringProp.GetValue(internalConnectionPropValue);

            return nameOrConnectionStringPropValue.ToString();
        }

        public static MappingModel GetTypeMetadataInfo(Type type)
        {
            MappingModel model = new MappingModel();

            model.AssemblyName = AssemblyInfo.GetAssemblyName(type.Assembly);

            model.Namespace = type.Namespace;

            model.EntityName = type.Name;

            return model;

        }
    }
}
