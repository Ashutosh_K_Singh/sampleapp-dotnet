﻿namespace CodeFirstClassLibrary
{
    using CrossCode.BLL.Domain.DBUsageStructure1;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    public class ContextLoader
    {

        /// <summary>
        /// Main function that contains other function to get mapping data into DTO
        /// </summary>
        /// <param name="contextType"></param>
        /// <param name="dbDetails"></param>
        /// <returns>returns DTO</returns>
        public static DBMappingDetails2 ProcessCodeFirstImp(Type contextType, DBMappingDetails2 dbDetails)
        {
            List<MappingModel> models = new List<MappingModel>();

            //1-Get all DbSet Types used to create tbale in Database
            Type[] genericTypes = GetGenericTypesOfDbSet(contextType);

            //2-Get Types Info
            GetEntityTypesInfo(genericTypes, models);

            //3-Get Table attribute value w.r.t to Entity name
            var toTableEntityName = GetEntityTableAttribute(genericTypes);

            //4-Get Dbbset varible name 
            var fromdbsetTableEntityName = GetDbSetTableEntityName(contextType);

            //5-Get final table names used for entity
            var tableMapping = UpdateTableMapingDictionary(toTableEntityName, fromdbsetTableEntityName);

            //6-Update Model for Table and entity mapping
            UpdateModelForTableMapping(tableMapping, models);

            //ConnectionString Name in Context Class
            GeConnectionStringNameSpecifedInContextClass(contextType);

            return GetDTOFromModel(models, dbDetails);
        }

        /// <summary>
        /// Fill DTO from internal mapping collection object
        /// </summary>
        /// <param name="models"></param>
        /// <param name="dbDetails"></param>
        /// <returns>DBMappingDetails2</returns>
        private static DBMappingDetails2 GetDTOFromModel(List<MappingModel> models, DBMappingDetails2 dbDetails)
        {
            DBMappingDetails2 dbusageStruct = new DBMappingDetails2();

            dbusageStruct.GenericORMmapDetails = new List<IDBMapItem>();

            foreach (var m in models)
            {
                var dbmapItem = new DBMapItem2
                {
                    TblNameKey = m.TableName,

                    MappedClass = m.EntityName,

                    MappedNS = m.Namespace,

                    MappedNamespace = m.Namespace,

                    MappedAssembly = m.AssemblyName,

                    CollectionOFMappedColumnProperties = m.CollectionOFMappedColumnProperties,

                    ColoumMappingDetails = new Dictionary<string, ColumnDetailsInfo>()
                };

                foreach (var rel in m.RelativeTypesInfo)
                {
                    var mappedColumns = rel.CollectionOFMappedColumnProperties.Where(k => m.CollectionOFMappedColumnProperties.ContainsKey(k.Key)).ToDictionary(x => x.Key, x => x.Value);
                    if (mappedColumns.Count > 0)
                    {
                        var propertyType = new ColumnDetailsInfo
                        {
                            TblNameKey = rel.TableName,

                            MappedClass = rel.EntityName,

                            MappedNamespace = rel.Namespace,

                            MappedAssembly = rel.AssemblyName,

                            CollectionOFMappedColumnProperties = mappedColumns
                        };
                        foreach (var colkey in mappedColumns)
                        {
                            if (!dbmapItem.ColoumMappingDetails.ContainsKey(colkey.Key))
                            {
                                dbmapItem.ColoumMappingDetails.Add(colkey.Key, propertyType);
                            }
                        }
                    }
                }

                dbusageStruct.GenericORMmapDetails.Add(dbmapItem);
            }

            return dbusageStruct;
        }

        /// <summary>
        /// Updates properties mapping changes with their respective entity
        /// </summary>
        /// <param name="columnAttributeMapping"></param>
        /// <param name="model"></param>
        private static void UpdateModelForColumnAttributeMapping(Dictionary<string, Dictionary<string, string>> columnAttributeMapping, MappingModel model)
        {
            model.CollectionOFMappedColumnProperties = columnAttributeMapping[model.EntityName];
        }

        /// <summary>
        /// Update table mapping changes with their respective entity
        /// </summary>
        /// <param name="tableMapping"></param>
        /// <param name="models"></param>
        private static void UpdateModelForTableMapping(Dictionary<string, string> tableMapping, List<MappingModel> models)
        {
            foreach (var m in models)
            {
                m.TableName = tableMapping[m.EntityName];
            }
        }
        private static void UpdateModelForTableMapping2(Dictionary<string, string> tableMapping, List<MappingModel> models)
        {
            foreach (var m in models)
            {
                m.TableName = tableMapping[m.EntityName];
            }
        }
        /// <summary>
        /// Get all properties type specifed in DB Set
        /// </summary>
        /// <param name="contexttype"></param>
        /// <returns>Array of types</returns>
        private static Type[] GetGenericTypesOfDbSet(Type contexttype)
        {
            var dbsetTypes = contexttype.GetProperties().Where(f => f.PropertyType.IsGenericType == true && f.PropertyType.IsInterface == false && f.PropertyType.Name.Contains("DbSet`1")).Select(s => s.PropertyType.GetGenericArguments()[0]).ToArray();

            //var prop = contexttype.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);

            return dbsetTypes;
        }

        #region Entity Table Info
        /// <summary>
        /// Get table attribute datanotation mapping declared on entity
        /// </summary>
        /// <param name="genericTypes"></param>
        /// <returns>table attribute mapping dictionary</returns>
        private static Dictionary<string, string> GetEntityTableAttribute(Type[] genericTypes)
        {
            Dictionary<string, string> tableNameWithEntityName = null;

            var customAttributData = genericTypes.Select(p => new
            {
                customdata = p.GetCustomAttributesData().Count > 0 ? p.GetCustomAttributesData()[0] : null,
                entityname = p.Name
            }).ToList();

            if (customAttributData.Count > 0)
            {
                /// Dictionary<this is entity class, this is table attribute> tableNameWithEntityName = new Dictionary<string, string>();

                tableNameWithEntityName = new Dictionary<string, string>();

                foreach (var attr in customAttributData)
                {
                    if (attr.customdata != null)
                    {
                        if (attr.customdata.ConstructorArguments.Count > 0)
                        {
                            tableNameWithEntityName.Add(attr.entityname, attr.customdata.ConstructorArguments[0].Value.ToString());
                        }
                        else
                        {
                            tableNameWithEntityName.Add(attr.entityname, string.Empty);
                        }
                    }
                    else
                    {
                        tableNameWithEntityName.Add(attr.entityname, string.Empty);
                    }
                }
            }

            return tableNameWithEntityName;
        }

        /// <summary>
        /// Get Dbset type's properties names used to create table
        /// </summary>
        /// <param name="contexttype"></param>
        /// <returns>types name and propeties name dictionary</returns>
        private static Dictionary<string, string> GetDbSetTableEntityName(Type contexttype)
        {
            var declaredTypes = contexttype.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
              .Where(f => f.PropertyType.IsGenericType == true && f.PropertyType.Name.Contains("DbSet`1")).ToDictionary(x => x.PropertyType.GetGenericArguments()[0].Name, x => x.Name);

            return declaredTypes;
        }

        /// <summary>
        /// Used to update table mapping changes
        /// </summary>
        /// <param name="todic"></param>
        /// <param name="fromdic"></param>
        /// <returns>table mapping dictionary</returns>
        private static Dictionary<string, string> UpdateTableMapingDictionary(Dictionary<string, string> todic, Dictionary<string, string> fromdic)
        {
            Dictionary<string, string> entitytablemapping = new Dictionary<string, string>();
            ///Filter todic bassed on key available in fromdic and value is empty
            var filteremptyValue = todic.Where(k => fromdic.ContainsKey(k.Key) && string.IsNullOrEmpty(k.Value)).ToDictionary(x => x.Key, x => x.Value);

            ///Filter todic bassed on key available in fromdic 
            var filternonemptyValue = todic.Where(k => fromdic.ContainsKey(k.Key) && !string.IsNullOrEmpty(k.Value)).ToDictionary(x => x.Key, x => x.Value);

            var dictionary = fromdic.Where(p => filteremptyValue.ContainsKey(p.Key)).ToDictionary(d => d.Key, d => d.Value);

            foreach (var dic in dictionary)
            {
                filteremptyValue[dic.Key] = dic.Value;
            }

            ///merge both dictionaries
            entitytablemapping = filternonemptyValue.Union(filteremptyValue).ToDictionary(x => x.Key, x => x.Value);

            return entitytablemapping;
        }
        #endregion

        #region Entity Property Custom Attribute Info

        /// <summary>
        /// Get column attribute datanotation mapping from main entity or drived entity which used to create table
        /// </summary>
        /// <param name="types"></param>
        /// <param name="bindingFlag"></param>
        /// <returns>dictionary with entity name and column mapping</returns>
        private static Dictionary<string, Dictionary<string, string>> GetEntityColumnAttributeDrivedClass(Type[] types, BindingFlags bindingFlag)
        {
            Dictionary<string, Dictionary<string, string>> entitywithpropertyattribute = new Dictionary<string, Dictionary<string, string>>();

            foreach (var type in types)
            {
                string entityName = type.Name;

                PropertyInfo[] allproperties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | bindingFlag);

                var properties = RemoveCollectionProperties(allproperties);

                Dictionary<string, string> propertyDictionary = new Dictionary<string, string>();

                foreach (var property in properties)
                {
                    var isCpxTyp = TypeCheck(property.PropertyType);

                    if (isCpxTyp)
                    {
                        var complexproperties = RemoveCollectionProperties(property.PropertyType.GetProperties()).ToArray();

                        PropertyInfo p2 = KeyAttributeAvailable(complexproperties, out bool KeyAvailable);

                        /// IF KeyAvailable is TRUE then just add key property mapping
                        /// IF KeyAvailable is FALSE then add all complex Type's properties to derived class
                        if (KeyAvailable)
                        {
                            FindPropertyAttributForComplexTypeDrived(propertyDictionary, p2, property.Name);
                        }
                        else
                        {
                            foreach (var p in property.PropertyType.GetProperties())
                            {
                                FindPropertyAttributForComplexTypeDrived(propertyDictionary, p, property.Name);
                            }
                        }
                    }
                    else
                    {
                        FindPropertyAttribute(propertyDictionary, property);
                    }
                }

                entitywithpropertyattribute.Add(entityName, propertyDictionary);
            }

            return entitywithpropertyattribute;
        }

        private static Dictionary<string, Dictionary<string, string>> GetEntityColumnAttributeDrivedClass2(Type[] types, BindingFlags bindingFlag)
        {
            Dictionary<string, Dictionary<string, string>> entitywithpropertyattribute = new Dictionary<string, Dictionary<string, string>>();

            foreach (var type in types)
            {
                string entityName = type.Name;

                PropertyInfo[] allproperties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | bindingFlag);

                var properties = RemoveCollectionProperties(allproperties);

                var simpleProperties = GetPropertiesByDeclaration(properties, DeclaredType.Simple);

                var complexProperties = GetPropertiesByDeclaration(properties, DeclaredType.Complex);

                Dictionary<string, string> propertyDictionary = new Dictionary<string, string>();

                /// Logic for simple type properties
                foreach (var property in simpleProperties)
                {
                    FindPropertyAttribute(propertyDictionary, property);
                }

                /// Logic for Complex type properties
                foreach (var ctp in complexProperties)
                {
                    Dictionary<string, string> complexDictionary = new Dictionary<string, string>();

                    bool isVirtual = IsPropertyVirtual(ctp);

                    var innerproperties = RemoveCollectionProperties(ctp.PropertyType.GetProperties()).ToArray();

                    PropertyInfo pinfo = KeyAttributeAvailable(innerproperties, out bool KeyAvailable);

                    if (KeyAvailable)
                    {
                        FindPropertyAttributForComplexTypeDrived(complexDictionary, pinfo, ctp.Name);
                    }
                    else
                    {
                        foreach (var p in innerproperties)
                        {
                            FindPropertyAttributForComplexTypeDrived(complexDictionary, p, ctp.Name, isVirtual);
                        }
                    }

                    if (isVirtual)
                    {
                        if (propertyDictionary.Where(k => complexDictionary.ContainsKey(k.Key)).Count() == 0)
                        {
                            foreach (var cd in complexDictionary)
                            {
                                if (!propertyDictionary.ContainsKey(cd.Key))
                                {
                                    propertyDictionary.Add(cd.Key, cd.Value);
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (var cd in complexDictionary)
                        {
                            if (!propertyDictionary.ContainsKey(cd.Key))
                            {
                                propertyDictionary.Add(cd.Key, cd.Value);
                            }
                        }
                    }
                }

                entitywithpropertyattribute.Add(entityName, propertyDictionary);
            }

            return entitywithpropertyattribute;
        }

        private static bool IsPropertyVirtual(PropertyInfo ctp)
        {
            var methodInfo = ctp.GetGetMethod();
            return methodInfo.IsVirtual;
        }

        private static List<PropertyInfo> GetPropertiesByDeclaration(List<PropertyInfo> properties, DeclaredType declaredType)
        {
            List<PropertyInfo> complexTypeProperties = new List<PropertyInfo>();
            foreach (var property in properties)
            {
                var iscomplex = TypeCheck(property.PropertyType);

                if (iscomplex && DeclaredType.Complex == declaredType)
                {
                    complexTypeProperties.Add(property);
                }
                else if (!iscomplex && DeclaredType.Simple == declaredType)
                {
                    complexTypeProperties.Add(property);
                }
            }

            return complexTypeProperties;
        }
        private static List<PropertyInfo> RemoveCollectionProperties(PropertyInfo[] properties)
        {
            List<PropertyInfo> noneCollectionProperties = new List<PropertyInfo>();
            foreach (var property in properties)
            {
                if (!IsICollectionProperties(property.PropertyType))
                {
                    noneCollectionProperties.Add(property);
                }
            }

            return noneCollectionProperties;
        }
        private static bool IsICollectionProperties(Type type)
        {
            if (type.UnderlyingSystemType != typeof(string))
            {
                if (type.GetInterface(nameof(System.Collections.IList)) != null || type.GetInterface(nameof(System.Collections.IEnumerable)) != null || type.GetInterface(nameof(System.Collections.ICollection)) != null)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Get column data attribute notation of base classes and complext types 
        /// </summary>
        /// <param name="types"></param>
        /// <param name="bindingFlag"></param>
        /// <returns>dictionary with entity name and column mapping</returns>
        private static Dictionary<string, Dictionary<string, string>> GetEntityColumnAttributeIncludingBaseTypes(Type[] types, BindingFlags bindingFlag)
        {
            Dictionary<string, Dictionary<string, string>> entitywithpropertyattribute = new Dictionary<string, Dictionary<string, string>>();
            foreach (var type in types)
            {
                string entityName = type.Name;

                PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | bindingFlag);

                Dictionary<string, string> propertyDictionary = new Dictionary<string, string>();

                foreach (var property in properties)
                {
                    var isCpxTyp = TypeCheck(property.PropertyType);

                    if (isCpxTyp)
                    {
                        foreach (var p in property.PropertyType.GetProperties())
                        {
                            //FindPropertyAttribut(propertyDictionary, p);
                            FindPropertyAttributForComplexType(propertyDictionary, p, property.Name);
                        }
                    }
                    else
                    {
                        FindPropertyAttribute(propertyDictionary, property);
                    }
                }

                entitywithpropertyattribute.Add(entityName, propertyDictionary);
            }

            return entitywithpropertyattribute;
        }

        /// <summary>
        /// Get complex type info used in entity
        /// </summary>
        /// <param name="type"></param>
        /// <param name="propertyName"></param>
        /// <returns>column attribute mapping dictionary</returns>
        private static Dictionary<string, string> GetComplexTypeInfo(Type type, string propertyName)
        {
            var properties = type.GetProperties();
            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (var p in properties)
            {
                FindPropertyAttributForComplexType(dic, p, propertyName);
            }

            return dic;
        }

        /// <summary>
        /// Get attributes defined on entity's properties
        /// </summary>
        /// <param name="propertyDictionary"></param>
        /// <param name="property"></param>
        private static void FindPropertyAttribute(Dictionary<string, string> propertyDictionary, PropertyInfo property)
        {
            var propertyname = property.Name;

            var attributeValue = property.GetCustomAttributesData().Select(x => x.ConstructorArguments).ToList();

            string[] attributeTypes = property.GetCustomAttributes(true).Select(x => x.GetType().Name).ToArray();

            if (Array.FindAll(attributeTypes, a => a.Equals("NotMappedAttribute")).Count() == 0)
            {
                if (Array.FindAll(attributeTypes, a => a.Equals("ColumnAttribute")).Count() > 0)
                {
                    var columnattribetype = property.GetCustomAttributes(true).Where(x => x.GetType().Name == "ColumnAttribute").ToList();

                    string keyColumn = DynamicPropertyGetter(columnattribetype, "Name");

                    DuplicateKeyCheck(propertyDictionary, keyColumn, keyColumn, 0, propertyname);
                }
                else
                {
                    propertyDictionary.Add(propertyname, propertyname);
                }
            }
        }

        /// <summary>
        /// Get key attribute defined on entity's properties
        /// </summary>
        /// <param name="propertyinfo"></param>
        /// <param name="keyavailable"></param>
        /// <returns>Property</returns>
        private static PropertyInfo KeyAttributeAvailable(PropertyInfo[] propertyinfo, out bool keyavailable)
        {
            PropertyInfo keyPropertyInfo = null;
            PropertyInfo idpropertyinfo = null;
            keyavailable = false;
            foreach (var property in propertyinfo)
            {
                var attributeValue = property.GetCustomAttributesData().Select(x => x.ConstructorArguments).ToList();

                var keyattribetype = property.GetCustomAttributes(true).Where(x => x.GetType().Name != "NotMappedAttribute" && x.GetType().Name == "KeyAttribute").ToList();

                if (keyattribetype.Count > 0)
                {
                    keyavailable = true;
                    keyPropertyInfo = property;
                    break;
                }
                else if (property.Name.ToUpper() == "ID" && property.PropertyType == typeof(int))
                {
                    keyavailable = true;
                    idpropertyinfo = property;
                }
            }

            if (keyPropertyInfo == null)
            {
                keyPropertyInfo = idpropertyinfo;
            }

            return keyPropertyInfo;
        }

        /// <summary>
        /// Get complex type property mapping used in main entity
        /// </summary>
        /// <param name="propertyDictionary"></param>
        /// <param name="property"></param>
        /// <param name="propName"></param>
        private static void FindPropertyAttributForComplexTypeDrived(Dictionary<string, string> propertyDictionary, PropertyInfo property, string propName, bool isVirtual = false)
        {
            var propertyname = property.Name;

            string keypropertyName = property.Name;

            string columnName = propName + "_" + keypropertyName;

            string[] attributeTypes = property.GetCustomAttributes(true).Select(x => x.GetType().Name).ToArray();

            if (Array.FindAll(attributeTypes, a => a.Equals("NotMappedAttribute")).Count() == 0)
            {
                if (Array.FindAll(attributeTypes, a => a.Equals("KeyAttribute")).Count() > 0)
                {
                    FindPropertyAttribute(propertyDictionary, property);
                }
                else if (property.Name.ToUpper() == "ID" && property.PropertyType == typeof(int))
                {
                    FindPropertyAttribute(propertyDictionary, property);
                }
                else if (Array.FindAll(attributeTypes, a => a.Equals("KeyAttribute")).Count() == 0 && Array.FindAll(attributeTypes, a => a.Equals("ColumnAttribute")).Count() > 0)
                {
                    FindPropertyAttribute(propertyDictionary, property);
                }
                else if (isVirtual)
                {
                    propertyDictionary.Add(propertyname, propertyname);
                }
                else
                {
                    var propertyUnderScorName = propName + "_" + propertyname;
                    propertyDictionary.Add(propertyUnderScorName, propertyname);
                }

            }
        }

        /// <summary>
        ///  Get complex type property mapping used in base entities
        /// </summary>
        /// <param name="propertyDictionary"></param>
        /// <param name="property"></param>
        /// <param name="propertyName"></param>
        private static void FindPropertyAttributForComplexType(Dictionary<string, string> propertyDictionary, PropertyInfo property, string propertyName)
        {
            var propertyname = property.Name;

            var attributeValue = property.GetCustomAttributesData().Select(x => x.ConstructorArguments).ToList();

            var attribeType = property.GetCustomAttributes(true).Where(x => x.GetType().Name != "NotMappedAttribute" && x.GetType().Name == "ColumnAttribute").ToList();

            if (attribeType.Count > 0)
            {
                string keyColumn = attributeValue[0].FirstOrDefault().Value.ToString();

                DuplicateKeyCheck(propertyDictionary, keyColumn, keyColumn, 0, propertyname);
            }
            else
            {
                var propertyUnderScorName = propertyName + "_" + propertyname;
                propertyDictionary.Add(propertyUnderScorName, propertyname);
            }
        }

        /// <summary>
        /// Check and update duplicate column attribute of an entity
        /// </summary>
        /// <param name="propertyDictionary"></param>
        /// <param name="key"></param>
        /// <param name="newKey"></param>
        /// <param name="idx"></param>
        /// <param name="propertyname"></param>
        /// <returns>true if duplicate column name exists</returns>
        private static bool DuplicateKeyCheck(Dictionary<string, string> propertyDictionary, string key, string newKey, int idx, string propertyname)
        {
            if (propertyDictionary.ContainsKey(newKey))
            {
                var counter = idx > 0 ? idx.ToString() : string.Empty;

                var newkey = key + counter;

                idx++;

                DuplicateKeyCheck(propertyDictionary, key, newkey, idx, propertyname);
            }
            else
            {
                propertyDictionary.Add(newKey, propertyname);
            }

            return false;
        }
        #endregion

        #region Entity Assembly and Namespace Info of drived types "Associated Types"

        /// <summary>
        /// Get entity info and bind to model
        /// </summary>
        /// <param name="types"></param>
        /// <param name="models"></param>
        private static void GetEntityTypesInfo(Type[] types, List<MappingModel> models)
        {
            foreach (var type in types)
            {
                var model = new MappingModel();

                model.AssemblyName = AssemblyInfo.GetAssemblyName(type.Assembly);

                model.Namespace = type.Namespace;

                model.EntityName = type.Name;

                ///var columnAttribute = GetEntityColumnAttributeIncludingBaseTypes(new Type[] { type }, BindingFlags.FlattenHierarchy);

                var columnAttribute = GetEntityColumnAttributeDrivedClass2(new Type[] { type }, BindingFlags.FlattenHierarchy);

                UpdateModelForColumnAttributeMapping(columnAttribute, model);

                ///For Complex Type Info
                GetRelativeTypesInfoFromComplexType(type, model);

                ///For Base Type Properties Info

                Type bt = type.BaseType;

                while (bt != null && bt != typeof(Object))
                {
                    var mdl = new MappingModel();

                    mdl.AssemblyName = AssemblyInfo.GetAssemblyName(bt.Assembly);

                    mdl.Namespace = bt.Namespace;

                    mdl.EntityName = bt.Name;

                    var relativeColumnAttribute = GetEntityColumnAttributeIncludingBaseTypes(new Type[] { bt }, BindingFlags.DeclaredOnly);

                    UpdateModelForColumnAttributeMapping(relativeColumnAttribute, mdl);

                    model.RelativeTypesInfo.Add(mdl);

                    bt = bt.BaseType;
                }

                models.Add(model);
            }
        }

        /// <summary>
        /// Get data attribute notaion properties info
        /// </summary>
        /// <param name="columnattribetype"></param>
        /// <param name="propertyname"></param>
        /// <returns></returns>
        private static dynamic DynamicPropertyGetter(List<object> columnattribetype, string propertyname)
        {
            return columnattribetype[0].GetType().GetProperty(propertyname).GetValue(columnattribetype[0], null);
        }

        /// <summary>
        /// Get or update model for complex type properties mappings
        /// </summary>
        /// <param name="type"></param>
        /// <param name="model"></param>
        private static void GetRelativeTypesInfoFromComplexType(Type type, MappingModel model)
        {
            var propertiesInfo = RemoveCollectionProperties(type.GetProperties());

            foreach (var property in propertiesInfo)
            {
                var propertName = property.Name;
                Type propertyType = property.PropertyType;

                var isct = TypeCheck(propertyType);
                if (isct)
                {
                    Dictionary<string, string> typeInfo = GetComplexTypeInfo(propertyType, propertName);

                    var keytypeInfo = typeInfo.Where(x => model.CollectionOFMappedColumnProperties.ContainsKey(x.Key)).Select(x => x).ToDictionary(d => d.Key, d => d.Value);
                    if (keytypeInfo != null)
                    {
                        model.RelativeTypesInfo.Add(new MappingModel
                        {
                            AssemblyName = propertyType.Assembly.GetName().Name,

                            Namespace = propertyType.Namespace,

                            EntityName = propertyType.Name,

                            CollectionOFMappedColumnProperties = keytypeInfo
                        });
                    }
                    else
                    {
                        model.RelativeTypesInfo.Add(new MappingModel
                        {
                            AssemblyName = propertyType.Assembly.GetName().Name,

                            Namespace = propertyType.Namespace,

                            EntityName = propertyType.Name,

                            CollectionOFMappedColumnProperties = typeInfo
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Check properties is complex type or not
        /// </summary>
        /// <param name="propertytype"></param>
        /// <returns>TRUE if complex type else FALSE</returns>
        private static bool TypeCheck(Type propertytype)
        {
            bool iscomplextype = false;
            Type retval = propertytype.UnderlyingSystemType;
            Type nullableType = Nullable.GetUnderlyingType(retval);

            if (nullableType == null)
            {
                if (System.Type.GetTypeCode(propertytype) == TypeCode.Object)
                {
                    iscomplextype = true;
                }
            }
            else if (System.Type.GetTypeCode(nullableType) == TypeCode.Object)
            {
                iscomplextype = true;
            }

            return iscomplextype;
        }
        #endregion

        /// <summary>
        /// Get connection string name specified in conext class constructor which drived from Db context
        /// </summary>
        /// <param name="type"></param>
        /// <returns>connection string name</returns>
        private static string GeConnectionStringNameSpecifedInContextClass(Type type)
        {
            var constructor = type.GetConstructors().FirstOrDefault(c => c.GetParameters().All(p => p.IsOptional));

            var parameters = constructor.GetParameters().Select(p => Type.Missing).ToArray();

            var instance = constructor.Invoke(BindingFlags.OptionalParamBinding | BindingFlags.InvokeMethod | BindingFlags.CreateInstance | BindingFlags.Instance, null, parameters, CultureInfo.InvariantCulture);

            var internalcontextprop = instance.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(p => p.Name == "InternalContext");

            var internalcontextpropvalue = internalcontextprop.GetValue(instance, null);

            var internalConnectionProp = internalcontextpropvalue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_internalConnection");

            var internalConnectionPropValue = internalConnectionProp.GetValue(internalcontextpropvalue);

            var nameOrConnectionStringProp = internalConnectionPropValue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_nameOrConnectionString");

            var nameOrConnectionStringPropValue = nameOrConnectionStringProp.GetValue(internalConnectionPropValue);

            return nameOrConnectionStringPropValue.ToString();
        }
    }
}