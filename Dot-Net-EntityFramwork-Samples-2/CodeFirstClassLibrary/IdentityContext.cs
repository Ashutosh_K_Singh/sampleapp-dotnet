﻿namespace CodeFirstClassLibrary
{
    using CrossCode.BLL.Domain.DBUsageStructure1;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    public class IdentityContext
    {
        /// <summary>
        /// Main function that contains other function to get mapping data into DTO
        /// </summary>
        /// <param name="contextType"></param>
        /// <param name="dbDetails"></param>
        /// <returns>returns DTO</returns>
        public static DBMappingDetails2 ProcessCodeFirstImp(Type contextType, DBMappingDetails2 dbDetails)
        {
            //0- Get ASPNETUSers Tables created by identity framwork
            var models = IdentityFrameworkTables.RunForASPNetUserTables(contextType);

            //1-Get all DbSet Types used to create tbale in Database
            Type[] genericTypes = Common.GetGenericTypesOfDbSet(contextType);

            //2-Get Types Info
            GetEntityTypesInfo(genericTypes, models);

            //3-Get Table attribute value w.r.t to Entity name
            var toTableEntityName = GetEntityTableAttribute(genericTypes);

            //4-Get Dbbset varible name 
            var fromdbsetTableEntityName = GetDbSetTableEntityName(contextType);

            //5-Get final table names used for entity
            var tableMapping = UpdateTableMapingDictionary(toTableEntityName, fromdbsetTableEntityName);


            MergeIdentityTables(models, tableMapping);

            //6-Update Model for Table and entity mapping
            UpdateModelForTableMapping(tableMapping, models);

            //ConnectionString Name in Context Class
            Common.GeConnectionStringNameFromDbContextCunstructor(contextType);

            return GetDTOFromModel(models, dbDetails);
        }

        private static void MergeIdentityTables(List<MappingModel> models, Dictionary<string, string> tableMapping)
        {
            foreach (var m in models)
            {
                if (!tableMapping.ContainsKey(m.EntityName))
                {
                    tableMapping.Add(m.EntityName, m.TableName);
                }
            }
        }

        /// <summary>
        /// Fill DTO from internal mapping collection object
        /// </summary>
        /// <param name="models"></param>
        /// <param name="dbDetails"></param>
        /// <returns>DBMappingDetails2</returns>
        private static DBMappingDetails2 GetDTOFromModel(List<MappingModel> models, DBMappingDetails2 dbDetails)
        {
            DBMappingDetails2 dbusageStruct = new DBMappingDetails2();

            dbusageStruct.GenericORMmapDetails = new List<IDBMapItem>();

            foreach (var m in models)
            {
                var dbmapItem = new DBMapItem2
                {
                    TblNameKey = m.TableName,

                    MappedClass = m.EntityName,

                    MappedNS = m.Namespace,

                    MappedNamespace = m.Namespace,

                    MappedAssembly = m.AssemblyName,

                    CollectionOFMappedColumnProperties = m.CollectionOFMappedColumnProperties,

                    ColoumMappingDetails = new Dictionary<string, ColumnDetailsInfo>()
                };

                foreach (var rel in m.RelativeTypesInfo)
                {
                    var mappedColumns = rel.CollectionOFMappedColumnProperties.Where(k => m.CollectionOFMappedColumnProperties.ContainsKey(k.Key)).ToDictionary(x => x.Key, x => x.Value);
                    if (mappedColumns.Count > 0)
                    {
                        var propertyType = new ColumnDetailsInfo
                        {
                            TblNameKey = rel.TableName,

                            MappedClass = rel.EntityName,

                            MappedNamespace = rel.Namespace,

                            MappedAssembly = rel.AssemblyName,

                            CollectionOFMappedColumnProperties = mappedColumns
                        };
                        foreach (var colkey in mappedColumns)
                        {
                            if (!dbmapItem.ColoumMappingDetails.ContainsKey(colkey.Key))
                            {
                                dbmapItem.ColoumMappingDetails.Add(colkey.Key, propertyType);
                            }
                        }
                    }
                }

                dbusageStruct.GenericORMmapDetails.Add(dbmapItem);
            }

            return dbusageStruct;
        }

        /// <summary>
        /// Updates properties mapping changes with their respective entity
        /// </summary>
        /// <param name="columnAttributeMapping"></param>
        /// <param name="model"></param>
        private static void UpdateModelForColumnAttributeMapping(Dictionary<string, Dictionary<string, string>> columnAttributeMapping, MappingModel model)
        {
            model.CollectionOFMappedColumnProperties = columnAttributeMapping[model.EntityName];
        }

        /// <summary>
        /// Update table mapping changes with their respective entity
        /// </summary>
        /// <param name="tableMapping"></param>
        /// <param name="models"></param>
        private static void UpdateModelForTableMapping(Dictionary<string, string> tableMapping, List<MappingModel> models)
        {
            foreach (var m in models)
            {
                m.TableName = tableMapping[m.EntityName];
            }
        }
        #region Entity Table Info
        /// <summary>
        /// Get table attribute datanotation mapping declared on entity
        /// </summary>
        /// <param name="genericTypes"></param>
        /// <returns>table attribute mapping dictionary</returns>
        private static Dictionary<string, string> GetEntityTableAttribute(Type[] genericTypes)
        {
            Dictionary<string, string> tableNameWithEntityName = null;

            var customAttributData = genericTypes.Select(p => new
            {
                customdata = p.GetCustomAttributesData().Count > 0 ? p.GetCustomAttributesData()[0] : null,
                entityname = p.Name
            }).ToList();

            if (customAttributData.Count > 0)
            {
                /// Dictionary<this is entity class, this is table attribute> tableNameWithEntityName = new Dictionary<string, string>();

                tableNameWithEntityName = new Dictionary<string, string>();

                foreach (var attr in customAttributData)
                {
                    if (attr.customdata != null)
                    {
                        if (attr.customdata.ConstructorArguments.Count > 0)
                        {
                            tableNameWithEntityName.Add(attr.entityname, attr.customdata.ConstructorArguments[0].Value.ToString());
                        }
                        else
                        {
                            tableNameWithEntityName.Add(attr.entityname, string.Empty);
                        }
                    }
                    else
                    {
                        tableNameWithEntityName.Add(attr.entityname, string.Empty);
                    }
                }
            }

            return tableNameWithEntityName;
        }

        /// <summary>
        /// Get Dbset type's properties names used to create table
        /// </summary>
        /// <param name="contexttype"></param>
        /// <returns>types name and propeties name dictionary</returns>
        private static Dictionary<string, string> GetDbSetTableEntityName(Type contexttype)
        {
            var declaredTypes = contexttype.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
              .Where(f => f.PropertyType.IsGenericType == true && f.PropertyType.Name.Contains("DbSet`1")).ToDictionary(x => x.PropertyType.GetGenericArguments()[0].Name, x => x.Name);

            return declaredTypes;
        }

        /// <summary>
        /// Used to update table mapping changes
        /// </summary>
        /// <param name="todic"></param>
        /// <param name="fromdic"></param>
        /// <returns>table mapping dictionary</returns>
        private static Dictionary<string, string> UpdateTableMapingDictionary(Dictionary<string, string> todic, Dictionary<string, string> fromdic)
        {
            Dictionary<string, string> entitytablemapping = new Dictionary<string, string>();
            ///Filter todic bassed on key available in fromdic and value is empty
            var filteremptyValue = todic.Where(k => fromdic.ContainsKey(k.Key) && string.IsNullOrEmpty(k.Value)).ToDictionary(x => x.Key, x => x.Value);

            ///Filter todic bassed on key available in fromdic 
            var filternonemptyValue = todic.Where(k => fromdic.ContainsKey(k.Key) && !string.IsNullOrEmpty(k.Value)).ToDictionary(x => x.Key, x => x.Value);

            var dictionary = fromdic.Where(p => filteremptyValue.ContainsKey(p.Key)).ToDictionary(d => d.Key, d => d.Value);

            foreach (var dic in dictionary)
            {
                filteremptyValue[dic.Key] = dic.Value;
            }

            ///merge both dictionaries
            entitytablemapping = filternonemptyValue.Union(filteremptyValue).ToDictionary(x => x.Key, x => x.Value);

            return entitytablemapping;
        }
        #endregion

        #region Entity Property Custom Attribute Info

        private static Dictionary<string, Dictionary<string, string>> GetEntityColumnAttributeDrivedClass2(Type[] types, BindingFlags bindingFlag)
        {
            Dictionary<string, Dictionary<string, string>> entitywithpropertyattribute = new Dictionary<string, Dictionary<string, string>>();

            foreach (var type in types)
            {
                string entityName = type.Name;

                PropertyInfo[] allproperties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | bindingFlag);

                var nonCollectionProperties = Common.ManageCollectionProperties(allproperties, false);

                var simpleProperties = Common.GetPropertiesByDeclaration(nonCollectionProperties, DeclaredType.Simple);

                var complexProperties = Common.GetPropertiesByDeclaration(nonCollectionProperties, DeclaredType.Complex);



                var propertyDictionary = Common.BuildPropertyMappingDictionary(simpleProperties, complexProperties);

                entitywithpropertyattribute.Add(entityName, propertyDictionary);
            }

            return entitywithpropertyattribute;
        }

        /// <summary>
        /// Get column data attribute notation of base classes and complext types 
        /// </summary>
        /// <param name="types"></param>
        /// <param name="bindingFlag"></param>
        /// <returns>dictionary with entity name and column mapping</returns>
        private static Dictionary<string, Dictionary<string, string>> GetEntityColumnAttributeIncludingBaseTypes(Type[] types, BindingFlags bindingFlag)
        {
            Dictionary<string, Dictionary<string, string>> entitywithpropertyattribute = new Dictionary<string, Dictionary<string, string>>();
            foreach (var type in types)
            {
                string entityName = type.Name;

                PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | bindingFlag);

                Dictionary<string, string> propertyDictionary = new Dictionary<string, string>();

                foreach (var property in properties)
                {
                    var isCpxTyp = Common.IsPropertyComplexType(property.PropertyType);

                    if (isCpxTyp)
                    {
                        foreach (var p in property.PropertyType.GetProperties())
                        {
                            //FindPropertyAttribut(propertyDictionary, p);
                            Common.FindPropertyAttributForComplexType(propertyDictionary, p, property.Name);
                        }
                    }
                    else
                    {
                        Common.FindPropertyAttribute(propertyDictionary, property);
                    }
                }

                entitywithpropertyattribute.Add(entityName, propertyDictionary);
            }

            return entitywithpropertyattribute;
        }
        #endregion

        #region Entity Assembly and Namespace Info of drived types "Associated Types"

        /// <summary>
        /// Get entity info and bind to model
        /// </summary>
        /// <param name="types"></param>
        /// <param name="models"></param>
        private static void GetEntityTypesInfo(Type[] types, List<MappingModel> models)
        {
            foreach (var type in types)
            {
                var metadataInfo = Common.GetTypeMetadataInfo(type);

                var columnAttribute = GetEntityColumnAttributeDrivedClass2(new Type[] { type }, BindingFlags.FlattenHierarchy);

                UpdateModelForColumnAttributeMapping(columnAttribute, metadataInfo);

                ///For Complex Type Info
                GetRelativeTypesInfoFromComplexType(type, metadataInfo);

                ///For Base Type Properties Info

                Type bt = type.BaseType;

                while (bt != null && bt != typeof(Object))
                {
                    var baseMetadataInfo = Common.GetTypeMetadataInfo(type);

                    var relativeColumnAttribute = GetEntityColumnAttributeIncludingBaseTypes(new Type[] { bt }, BindingFlags.DeclaredOnly);

                    UpdateModelForColumnAttributeMapping(relativeColumnAttribute, baseMetadataInfo);

                    metadataInfo.RelativeTypesInfo.Add(baseMetadataInfo);

                    bt = bt.BaseType;
                }

                models.Add(metadataInfo);
            }
        }

        /// <summary>
        /// Get or update model for complex type properties mappings
        /// </summary>
        /// <param name="type"></param>
        /// <param name="mappingModel"></param>
        private static void GetRelativeTypesInfoFromComplexType(Type type, MappingModel mappingModel)
        {
            var noneCollectionProperties = Common.ManageCollectionProperties(type.GetProperties(), false);

            foreach (var complexProperty in noneCollectionProperties)
            {
                var complexPropertyName = complexProperty.Name;

                bool isPropertyVirtual = complexProperty.GetGetMethod().IsVirtual;

                Type propertyType = complexProperty.PropertyType;

                if (Common.IsPropertyComplexType(propertyType))
                {
                    var properties = Common.ManageCollectionProperties(propertyType.GetProperties(), false);

                    Dictionary<string, string> mappedPropertyInfo = null;

                    Dictionary<string, string> typeInfo = new Dictionary<string, string>();

                    foreach (var p in properties)
                    {
                        Common.FindPropertyAttributForComplexType(typeInfo, p, complexPropertyName, isPropertyVirtual);

                        mappedPropertyInfo = typeInfo.Where(x => mappingModel.CollectionOFMappedColumnProperties.ContainsKey(x.Key)).Select(x => x).ToDictionary(d => d.Key, d => d.Value);
                    }

                    var model = new MappingModel();
                    model.AssemblyName = propertyType.Assembly.GetName().Name;
                    model.Namespace = propertyType.Namespace;
                    model.EntityName = propertyType.Name;

                    if (mappedPropertyInfo.Count > 0)
                    {
                        model.CollectionOFMappedColumnProperties = mappedPropertyInfo;
                        mappingModel.RelativeTypesInfo.Add(model);
                    }
                    else if (!isPropertyVirtual)
                    {
                        model.CollectionOFMappedColumnProperties = typeInfo;
                        mappingModel.RelativeTypesInfo.Add(model);
                    }
                }
            }
        }
        #endregion

        //If property is virtual and key propertyName exists in type

    }
}
