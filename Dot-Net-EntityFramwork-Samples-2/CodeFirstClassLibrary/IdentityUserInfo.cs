﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CodeFirstClassLibrary
{
    public class IdentityFrameworkTables
    {
        public static List<MappingModel> RunForASPNetUserTables(Type contextType)
        {
            var type = GetContextTypeBase(contextType);

            return GetIdentityContextMappingInfo(type);
        }

        private static Dictionary<string, string> IdentityDbContextTable()
        {
            var dic = new Dictionary<string, string>();

            dic.Add("IdentityUser", "AspNetUsers");

            dic.Add("IdentityUserRole", "AspNetUserRoles");

            dic.Add("IdentityUserLogin", "AspNetUserLogins");

            dic.Add("IdentityUserClaim", "AspNetUserClaims");

            dic.Add("IdentityRole", "AspNetRoles");

            dic.Add("TUser", "AspNetUsers");

            return dic;
        }

        private static Type GetContextTypeBase(Type contextType)
        {
            Type type = null;

            Type baseType = contextType.BaseType;

            while (baseType != typeof(Object) && baseType != null)
            {
                if (baseType.Name == "IdentityDbContext`1")
                {
                    return baseType;
                }
                baseType = baseType.BaseType;
            }

            return type;
        }

        private static List<MappingModel> GetIdentityContextMappingInfo(Type identitycontextType)
        {
            List<MappingModel> mappingModel = null;

            List<PropertyInfo> identitycontextProperties = null;

            Type genericTypeArg = null;

            /// Get only virtual properties
            identitycontextProperties = identitycontextType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).Where(p => p.GetGetMethod().IsVirtual).ToList();

            genericTypeArg = identitycontextType.GetGenericArguments().FirstOrDefault();

            foreach (var idctxp in identitycontextProperties)
            {
                var propertyType = idctxp.PropertyType;

                var propGenericTypeArg = propertyType.GetGenericArguments().FirstOrDefault();

                /// Check For Identity User

                if (propGenericTypeArg == genericTypeArg)
                {
                    mappingModel = new List<MappingModel>();

                    var GenericTypeUser = ProcessGenericType(genericTypeArg, mappingModel);

                    var ASPUserTable = UpdateGenericTypForASPNetUserTable(GenericTypeUser);

                    var IdentityUser = ProcessIdentityUserTypes(genericTypeArg.BaseType, mappingModel);

                    var updatedIdentityUserTables = UpdateIdentityUserForAspNetUserTable(IdentityUser);

                    BuildMappingModel(ASPUserTable, mappingModel);

                    BuildMappingModel(updatedIdentityUserTables, mappingModel);
                }
            }

            return mappingModel;
        }

        public static void BuildMappingModel(Dictionary<Dictionary<string, string>, Dictionary<string, string>> mappingData, List<MappingModel> mappingModel)
        {

            foreach (var columns in mappingData)
            {
                foreach (var map in columns.Key)
                {
                    foreach (var model in mappingModel)
                    {
                        if (model.EntityName == map.Key)
                        {
                            model.CollectionOFMappedColumnProperties = columns.Value;

                            model.TableName = map.Value;
                        }
                    }
                }
            }

        }

        private static Dictionary<Dictionary<string, string>, Dictionary<string, string>> UpdateIdentityUserForAspNetUserTable(Dictionary<Dictionary<string, string>, Dictionary<string, string>> identityUser)
        {
            var aspnetTable = IdentityDbContextTable();

            Dictionary<string, string> entitytablemapping = new Dictionary<string, string>();

            foreach (var pair in identityUser.Keys)
            {
                var mapped = aspnetTable.Where(k => pair.ContainsKey(k.Key)).FirstOrDefault();

                pair[mapped.Key] = mapped.Value;
            }

            return identityUser;
        }

        private static Dictionary<Dictionary<string, string>, Dictionary<string, string>> UpdateGenericTypForASPNetUserTable(Dictionary<Dictionary<string, string>, Dictionary<string, string>> genericTypeUser)
        {
            Dictionary<Dictionary<string, string>, Dictionary<string, string>> buildDictionary = new Dictionary<Dictionary<string, string>, Dictionary<string, string>>();

            var aspnetTable = IdentityDbContextTable();

            Dictionary<string, string> entitytablemapping = new Dictionary<string, string>();

            foreach (var pair in genericTypeUser)
            {
                var mapped = aspnetTable.Where(x => pair.Key.ContainsValue(x.Key)).Select(x => x).FirstOrDefault();

                var key = pair.Key.Select(x => x.Key).FirstOrDefault();

                entitytablemapping.Add(key, mapped.Value);

                buildDictionary.Add(entitytablemapping, pair.Value);
            }

            return buildDictionary;
        }

        private static Dictionary<Dictionary<string, string>, Dictionary<string, string>> ProcessIdentityUserTypes(Type identityUserType, List<MappingModel> mappingModel)
        {
            PropertyInfo[] allPropertyInfo = identityUserType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly).ToArray();

            var Collectionproperties = Common.ManageCollectionProperties(allPropertyInfo, true);

            Dictionary<Dictionary<string, string>, Dictionary<string, string>> identityUserpropertyattribute = new Dictionary<Dictionary<string, string>, Dictionary<string, string>>();

            foreach (var property in Collectionproperties)
            {
                var type = property.PropertyType.GetGenericArguments().FirstOrDefault();

                var entityMapping = new Dictionary<string, string>();

                entityMapping.Add(type.Name, type.Name);

                var simpleProperties = Common.GetPropertiesByDeclaration(type.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance).ToList(), DeclaredType.Simple);

                var complexProperties = Common.GetPropertiesByDeclaration(simpleProperties, DeclaredType.Complex);

                Dictionary<string, string> propertyDictionary = Common.BuildPropertyMappingDictionary(simpleProperties, complexProperties);

                var model = Common.GetTypeMetadataInfo(type);

                mappingModel.Add(model);

                identityUserpropertyattribute.Add(entityMapping, propertyDictionary);

            }

            return identityUserpropertyattribute;
        }

        private static Dictionary<Dictionary<string, string>, Dictionary<string, string>> ProcessGenericType(Type genericTypeArg, List<MappingModel> MappingModel)
        {
            var entityNameWithBaseName = new Dictionary<string, string>();

            entityNameWithBaseName.Add(genericTypeArg.Name, genericTypeArg.BaseType.Name);

            Dictionary<Dictionary<string, string>, Dictionary<string, string>> appUserwithpropertyattribute = new Dictionary<Dictionary<string, string>, Dictionary<string, string>>();

            PropertyInfo[] allPropertyInfo = genericTypeArg.GetProperties(BindingFlags.Instance | BindingFlags.Public).ToArray();

            var noneCollectionproperties = Common.ManageCollectionProperties(allPropertyInfo, false);

            var simpleProperties = Common.GetPropertiesByDeclaration(noneCollectionproperties, DeclaredType.Simple);

            var complexProperties = Common.GetPropertiesByDeclaration(noneCollectionproperties, DeclaredType.Complex);

            Dictionary<string, string> propertyDictionary = Common.BuildPropertyMappingDictionary(simpleProperties, complexProperties);

            var model = Common.GetTypeMetadataInfo(genericTypeArg);

            MappingModel.Add(model);

            appUserwithpropertyattribute.Add(entityNameWithBaseName, propertyDictionary);

            return appUserwithpropertyattribute;
        }
    }
}
