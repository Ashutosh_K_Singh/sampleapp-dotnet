﻿namespace CodeFirstClassLibrary
{
    using System.Collections.Generic;
    using System.Reflection;
    public class MappingModel
    {
        public MappingModel()
        {
            RelativeTypesInfo = new List<MappingModel>();
        }
        public string AssemblyName { get; set; }
        public string Namespace { get; set; }
        public string TableName { get; set; }
        public string EntityName { get; set; }
        public Dictionary<string, string> CollectionOFMappedColumnProperties { get; set; } //Dictionary<columnName, EntityPropertyName>
        public List<MappingModel> RelativeTypesInfo { get; set; }
    }
    public class AssemblyInfo
    {
        /// <summary>
        /// Extract assembly name from Assembly structure from reflection
        /// </summary>
        /// <param name="assem">Assemlby reflection structure</param>
        /// <returns>Assembly name</returns>
        public static string GetAssemblyName(Assembly assem)
        {
            string assemblyName = string.Empty;

            assemblyName = assem.ManifestModule.Name;

            if (assemblyName == "<Unknown>")
            {
                assemblyName = assem.ManifestModule.ScopeName;
            }

            return assemblyName;
        }
    }
    public enum DeclaredType
    {
        Simple = 0,
        Complex = 1,
        Collection = 2,
        NoneCollection = 3
    }
}
