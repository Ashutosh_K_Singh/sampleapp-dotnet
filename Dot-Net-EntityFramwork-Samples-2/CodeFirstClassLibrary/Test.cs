﻿using CrossCode.BLL.Domain.DBUsageStructure1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CodeFirstClassLibrary
{
    public class Test
    {
        public static DBMappingDetails2 ExtractDll(string path)
        {
            Assembly projectAssembly = Assembly.LoadFrom(path);

            //1-Get all types from assembly
            Type[] types = projectAssembly.GetTypes().Where(x => x.IsSealed == false && x.IsInterface == false).Select(x => x).ToArray();

            DBMappingDetails2 dbDetails = new DBMappingDetails2();

            DBMappingDetails2 DTO = null;

            foreach (var t in types)
            {
                if (t.BaseType.FullName == "System.Data.Entity.DbContext")
                {
                    DTO = ContextLoader.ProcessCodeFirstImp(t, dbDetails);
                }
                else if (t.BaseType.Name == "IdentityDbContext`1")
                {
                    DTO = IdentityContext.ProcessCodeFirstImp(t, dbDetails);
                }
            }

            return DTO;
        }
    }
}
