﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF5CodeFirstClassLibrary
{
    public abstract class BookDetail : BaseEntity
    {
        [Column("BookWriter", TypeName = "ntext")]
        public string Author { get; set; }
        public string RelaseDate { get; set; }
        public string Publication { get; set; }
    }
}
