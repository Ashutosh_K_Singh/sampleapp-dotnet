﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF5CodeFirstClassLibrary
{
    public class Course
    {

        public int MyPropertyID { get; set; }
        //public int MyProperty { get; set; }
        //public int ID { get; set; }
        public string CourseName { get; set; }
    }
}
