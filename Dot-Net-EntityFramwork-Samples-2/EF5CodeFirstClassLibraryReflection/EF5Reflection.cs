﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Design;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EF5CodeFirstClassLibraryReflection
{
    public class EF5Reflection
    {
        public static void GetAllMappingsEF5()
        {

        
            string rootPath = CreateMigrationDirectory(@"C:\EF5CodeFirst");
            /*Looad code first*/

            var assemblyPath = @"C:\Users\rajneesh.kumar\source\repos\MasterSolution\EF5CodeFirstClassLibrary\bin\Debug";

            var assemblyName = string.Format(@"{0}\{1}.{2}", assemblyPath, "EF5CodeFirstClassLibrary", "dll");

            var assembly = Assembly.LoadFrom(assemblyName);

            var type = assembly.GetType("EF5CodeFirstClassLibrary.SampleDBContext");

            Type genericClass = typeof(DbMigrationsConfiguration<>);

            //MakeGenericType is badly named

            Type constructedClass = genericClass.MakeGenericType(type);

            object created = Activator.CreateInstance(constructedClass);

            ////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////

            var config = (DbMigrationsConfiguration)created;

            config.AutomaticMigrationsEnabled = true;

            var migrator = new DbMigrator(config);

            //Get code migration//
            var scaffolder = new MigrationScaffolder(migrator.Configuration);

            ScaffoldedMigration migration = scaffolder.Scaffold("codeMigration");

            var migrationFile = System.IO.Path.Combine(rootPath, migration.Directory, migration.MigrationId);

            var userCodeFile = migrationFile + ".cs";

            File.WriteAllText(userCodeFile, migration.UserCode);
            Console.WriteLine("Entity Migration generated.");
            //Get Db script//
            /*
            var scriptor = new MigratorScriptingDecorator(migrator);

            string script = scriptor.ScriptUpdate(sourceMigration: null, targetMigration: null);

            var SqlScriptFile = migrationFile + ".sql";

            File.WriteAllText(SqlScriptFile, script);
            Console.WriteLine("Sql Script  for migration generated.");
            */
            //Get Edmx Document//
            var _currenModelProp = migrator.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Single(m => m.Name == "_currentModel");

            var _currenModelValueXDOC = (XDocument)_currenModelProp.GetValue(migrator);

            var edmxFile = migrationFile + ".xml";

            File.WriteAllText(edmxFile, _currenModelValueXDOC.ToString());
            Console.WriteLine("Code First EDMX generated.");
            Console.WriteLine("Migration path {0} ", rootPath);
        }


        static string CreateMigrationDirectory(string path)
        {
            string migrationFilePath = path + "\\Migrations";
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(migrationFilePath);
                }
                catch (Exception ex)
                {
                    // handle them here
                }
            }
            return path;
        }
    }
}
