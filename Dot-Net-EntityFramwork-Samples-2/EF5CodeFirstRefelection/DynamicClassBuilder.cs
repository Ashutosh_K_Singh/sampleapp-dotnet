﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace EF5CodeFirstRefelection
{
    public class DynamicClassBuilder
    {
        AssemblyName asemblyName;
        public DynamicClassBuilder(string ClassName)
        {
            this.asemblyName = new AssemblyName(ClassName);
        }
        /// <summary>
        /// To generate the dynamic class
        /// </summary>
        /// <param name="PropertyNames">pass class property names</param>
        /// <param name="Types">pass types of property</param>
        /// <param name="InterfaceType">pass interface type if need to add</param>
        /// <returns>Object</returns>
        public object CreateObject(string[] PropertyNames, Type[] Types, Type InterfaceType = null)
        {
            if (PropertyNames.Length != Types.Length)
            {
                Console.WriteLine("The number of property names should match their corresopnding types number");
            }

            TypeBuilder DynamicClass = this.CreateClass();

            this.CreateConstructor(DynamicClass);

            for (int ind = 0; ind < PropertyNames.Count(); ind++)
            {
                CreateProperty(DynamicClass, PropertyNames[ind], Types[ind]);
            }

            if (InterfaceType != null)
            {
                AddInteraceToDynamicClass(InterfaceType, DynamicClass);
            }
            Type type = DynamicClass.CreateType();

            return Activator.CreateInstance(type);
        }
        /// <summary>
        /// Add interface to dynamic generated class
        /// </summary>
        /// <param name="InterfaceType"></param>
        /// <param name="DynamicClass"></param>
        private static void AddInteraceToDynamicClass(Type InterfaceType, TypeBuilder DynamicClass)
        {
            // Mark the class as implementing 'InterfaceType' interface.
            DynamicClass.AddInterfaceImplementation(InterfaceType);

            MethodBuilder methodBuilder = DynamicClass.DefineMethod("SayHello",
                                MethodAttributes.Public | MethodAttributes.Virtual,
                                null,
                                null);
            MethodInfo[] methods = InterfaceType.GetMethods();
            // Generate IL for 'MethodA' method.
            ILGenerator IL = methodBuilder.GetILGenerator();
            //IL.EmitWriteLine("This is value:");
            //IL.Emit(OpCodes.Ldarg_0);      // Load "this"
            //IL.Emit(OpCodes.Ldarg_1);      // Load "value" onto the stack                          

            //IL.Emit(OpCodes.Ldarg_0);
            //IL.Emit(OpCodes.Callvirt, InterfaceType.GetMethod("SayHello"));
            IL.Emit(OpCodes.Nop);
            //IL.EmitCall(OpCodes.Call, methods[0], new Type[1] { typeof(string) });
            


            //MethodInfo methodA = InterfaceType.GetMethod("MethodA");

            foreach (MethodInfo method in methods)
            {
                DynamicClass.DefineMethodOverride(methodBuilder, method);
            }

        }

        private TypeBuilder CreateClass()
        {
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(this.asemblyName, AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("MainModule");
            TypeBuilder typeBuilder = moduleBuilder.DefineType(this.asemblyName.FullName
                                , TypeAttributes.Public |
                                TypeAttributes.Class |
                                TypeAttributes.AutoClass |
                                TypeAttributes.AnsiClass |
                                TypeAttributes.BeforeFieldInit |
                                TypeAttributes.AutoLayout
                                , null);
            return typeBuilder;
        }
        private void CreateConstructor(TypeBuilder typeBuilder)
        {
            typeBuilder.DefineDefaultConstructor(MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName|MethodAttributes.Family);
         
        }
        private void CreateProperty(TypeBuilder typeBuilder, string propertyName, Type propertyType)
        {
            FieldBuilder fieldBuilder = typeBuilder.DefineField("_" + propertyName, propertyType, FieldAttributes.Private);

            PropertyBuilder propertyBuilder = typeBuilder.DefineProperty(propertyName, PropertyAttributes.HasDefault, propertyType, null);
            MethodBuilder getPropMthdBldr = typeBuilder.DefineMethod("get_" + propertyName, MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, propertyType, Type.EmptyTypes);
            ILGenerator getIl = getPropMthdBldr.GetILGenerator();

            getIl.Emit(OpCodes.Ldarg_0);
            getIl.Emit(OpCodes.Ldfld, fieldBuilder);
            getIl.Emit(OpCodes.Ret);

            MethodBuilder setPropMthdBldr = typeBuilder.DefineMethod("set_" + propertyName,
                  MethodAttributes.Public |
                  MethodAttributes.SpecialName |
                  MethodAttributes.HideBySig,
                  null, new[] { propertyType });

            ILGenerator setIl = setPropMthdBldr.GetILGenerator();
            Label modifyProperty = setIl.DefineLabel();
            Label exitSet = setIl.DefineLabel();

            setIl.MarkLabel(modifyProperty);
            setIl.Emit(OpCodes.Ldarg_0);
            setIl.Emit(OpCodes.Ldarg_1);
            setIl.Emit(OpCodes.Stfld, fieldBuilder);

            setIl.Emit(OpCodes.Nop);
            setIl.MarkLabel(exitSet);
            setIl.Emit(OpCodes.Ret);

            propertyBuilder.SetGetMethod(getPropMthdBldr);
            propertyBuilder.SetSetMethod(setPropMthdBldr);
        }
    }
}
