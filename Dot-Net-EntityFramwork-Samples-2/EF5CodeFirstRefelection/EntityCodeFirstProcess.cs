﻿
using CrossCode.BLL.Domain.DBUsageStructure1;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace EF5CodeFirstRefelection
{
    public class EntityCodeFirstProcess
    {
        public static Type GetContextType(Type[] types)
        {
            var contextType = types.Where(t => t.BaseType.FullName == "System.Data.Entity.DbContext" || t.BaseType.Name == "IdentityDbContext`1").Select(x => x).FirstOrDefault();
            return contextType;
        }
        public static void RunProcess(Type type, string efASMPath)
        {
            try
            {   //1               
                string xmledmx = GetCodeFirstProjectEDMX(type, efASMPath);
                //2
                var edmxModles = EDMXModels(xmledmx);

                string entityContainerclassName = string.Empty;

                string nameSpacevalue = string.Empty;

                //3
                XMLHelper.GetNamespace(edmxModles.ConceptualModels, out entityContainerclassName, out nameSpacevalue);
                //4
                var entityMapping = XMLHelper.GetTablesMapping(edmxModles.StorageModels, null);
                //5
                var updatedmapping = XMLHelper.GetDBNORMMapping(edmxModles.MappingsModels, entityMapping, "EFCF");

                DBMappingDetails2 dbusageStruct = new DBMappingDetails2();
                //6
                XMLHelper.Map(updatedmapping, dbusageStruct, type);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }

        private static string GetCodeFirstProjectEDMX(Type ContextType, string EntityFrameworkPath)
        {
            string xmlEdmx = string.Empty;
            try
            {

                /*Load Entity Frmaework*/
                var entityframeworkAssembly = Assembly.LoadFrom(EntityFrameworkPath);

                Type DbMigrationsConfigurationType = entityframeworkAssembly.GetType("System.Data.Entity.Migrations.DbMigrationsConfiguration`1");

                Type DbMigratortype = entityframeworkAssembly.GetType("System.Data.Entity.Migrations.DbMigrator");

                Type constructedClass = DbMigrationsConfigurationType.MakeGenericType(ContextType);

                dynamic config = Activator.CreateInstance(constructedClass);

                config.AutomaticMigrationsEnabled = false;

                var migrator = Activator.CreateInstance(DbMigratortype, new object[] { config });

                var _currenModelProp = migrator.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Single(m => m.Name == "_currentModel");

                var _currenModelValueXDOC = (XDocument)_currenModelProp.GetValue(migrator);

                xmlEdmx = _currenModelValueXDOC.ToString();

                var edmxFile = @"C:\EFCodeFirst\edmx.txt";

                File.WriteAllText(edmxFile, _currenModelValueXDOC.ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return xmlEdmx;
        }
        public static List<XmlNode> WriteConnectionToAppConfig(string AppConfigPath, string ProjectAppConfigPath)
        {
            List<XmlNode> connectionStringNodes = new List<XmlNode>();
            try
            {
                string XPath = "//configuration//connectionStrings";

                //Load App.config To Append//
                XmlDocument appDoc = new XmlDocument();
                appDoc.Load(AppConfigPath);
                XmlNode parentNode = appDoc.SelectSingleNode(XPath);

                //Load Project App.Config //
                XmlDocument projectDoc = new XmlDocument();
                projectDoc.Load(ProjectAppConfigPath);
                XmlNode nodes = projectDoc.SelectSingleNode(XPath);

                //Add connection String from project's app.config//
                foreach (XmlNode node in nodes)
                {
                    connectionStringNodes.Add(node);
                    XmlNode importNode = appDoc.ImportNode(node, true);
                    foreach (XmlNode chNode in parentNode.ChildNodes)
                    {
                        if (chNode.NodeType == node.NodeType)
                        {
                            if (chNode.Attributes["name"].Value == node.Attributes["name"].Value)
                            {
                                RemoveConnectionString(AppConfigPath, new List<XmlNode> { chNode });
                            }
                        }
                    }
                    parentNode.InsertBefore((XmlElement)importNode, parentNode.FirstChild);
                }
                appDoc.Save(AppConfigPath);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return connectionStringNodes;
        }
        public static void RemoveConnectionString(string AppConfigPath, List<XmlNode> connectionStringNodes)
        {
            try
            {
                string XPath = "//configuration//connectionStrings";

                //Load App.config To Append//
                XmlDocument appDoc = new XmlDocument();
                appDoc.Load(AppConfigPath);
                XmlNode appNode = appDoc.SelectSingleNode(XPath);

                //Add connection String from project's app.config//
                foreach (XmlNode node in connectionStringNodes)
                {
                    var name = node.Attributes["name"].Value;
                    foreach (XmlNode chnode in appNode.ChildNodes)
                    {
                        if (chnode.Attributes["name"].Value == name)
                        {
                            appNode.RemoveChild(chnode);
                        }
                    }
                }
                appDoc.Save(AppConfigPath);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        private static string AddProjectConnetionStringToAppConfig(string connnectionStringName, string AppConfigPath)
        {
            string name = string.Empty;
            if (connnectionStringName.Contains("="))
            {
                string[] conname = connnectionStringName.Split('=');
                name = conname[1];
            }
            else
            {
                name = connnectionStringName;
            }
            XmlDocument doc = new XmlDocument();

            doc.Load(AppConfigPath);

            XmlNode parentNode = doc.SelectSingleNode("//configuration//connectionStrings");

            //Create a new node.
            XmlElement childNode = doc.CreateElement("add");

            childNode.SetAttribute("name", name);

            childNode.SetAttribute("connectionString", "Data Source=SEZ3CROSSCODE\\SQLEXPRESS;Initial Catalog=SampleDB2;Integrated Security=True");

            childNode.SetAttribute("providerName", "System.Data.SqlClient");

            parentNode.InsertBefore(childNode, parentNode.FirstChild);

            doc.Save(AppConfigPath);

            return name;

        }
        private static string GeConnectionStringNameSpecifedInContextType(Type type)
        {
            string connetionStringName = string.Empty;
            try
            {
                var constructor = type.GetConstructors().FirstOrDefault(c => c.GetParameters().All(p => p.IsOptional));

                var parameters = constructor.GetParameters().Select(p => Type.Missing).ToArray();

                var instance = constructor.Invoke(BindingFlags.OptionalParamBinding | BindingFlags.InvokeMethod | BindingFlags.CreateInstance | BindingFlags.Instance, null, parameters, CultureInfo.InvariantCulture);

                var InternalContextProp = instance.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(p => p.Name == "InternalContext");

                var InternalContextPropValue = InternalContextProp.GetValue(instance, null);

                var _internalConnectionProp = InternalContextPropValue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_internalConnection");

                var _internalConnectionPropValue = _internalConnectionProp.GetValue(InternalContextPropValue);

                var _nameOrConnectionStringProp = _internalConnectionPropValue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_nameOrConnectionString");

                var _nameOrConnectionStringPropValue = _nameOrConnectionStringProp.GetValue(_internalConnectionPropValue);

                //var connectionStringName = InternalContextPropValue.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Single(p => p.Name == "ConnectionStringName").GetValue(InternalContextPropValue);

                connetionStringName = _nameOrConnectionStringPropValue.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return connetionStringName;
        }
        private static EdmxModel EDMXModels(string xmlEdmx)
        {
            EdmxModel edmxModel = new EdmxModel();
            try
            {
                XmlDocument appDoc = new XmlDocument();

                appDoc.LoadXml(xmlEdmx);

                edmxModel.ConceptualModels = appDoc.GetElementsByTagName("ConceptualModels")[0].OuterXml;

                edmxModel.MappingsModels = appDoc.GetElementsByTagName("Mappings")[0].OuterXml;

                edmxModel.StorageModels = appDoc.GetElementsByTagName("StorageModels")[0].OuterXml;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return edmxModel;
        }

        public static string RunEdmxProcess(string appConfigPath, string projectConfigPath, string projecAssemblypath, string efASMPath)
        {
            dynamic nodes = null;
            string edmxdata = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(appConfigPath))
                {
                    appConfigPath = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
                }

                nodes = WriteConnectionToAppConfig(appConfigPath, projectConfigPath);

                Assembly projectAssembly = Assembly.LoadFrom(projecAssemblypath);

                //1-Get all types from assembly
                Type[] types = projectAssembly.GetTypes();
                var type = GetContextType(types);

                edmxdata = GetCodeFirstProjectEDMX(type, efASMPath);

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                EntityCodeFirstProcess.RemoveConnectionString(appConfigPath, nodes);
            }
            return edmxdata;
        }
    }
    public class EdmxModel
    {
        public string ConceptualModels { get; set; }
        public string MappingsModels { get; set; }
        public string StorageModels { get; set; }
    }

}
