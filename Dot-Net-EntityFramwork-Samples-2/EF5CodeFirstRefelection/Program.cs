﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;




namespace EF5CodeFirstRefelection
{
    public class Program1
    {
        public static void Main(string[] args)
        {

            string projectConfigPath = @"C:\Publish\CodeFirstSample\web.config";

            string appConfigPath = @"C:\Users\rajneesh.kumar\source\repos\MasterSolution\EF5CodeFirstRefelection\bin\Debug\EF5CodeFirstRefelection.exe.config";
            dynamic nodes = null;
            try
            {
                //Write Connection String to app.config
                nodes = EntityCodeFirstProcess.WriteConnectionToAppConfig(appConfigPath, projectConfigPath);
                //string[] fileArray = Directory.GetFiles(@"c:\Dir\", "*.jpg", SearchOption.TopDirectoryOnly);
                //Get EDMX XML
                //var projASM = Assembly.LoadFrom(@"C:\Users\rajneesh.kumar\source\repos\MasterSolution\EF5CodeFirstClassLibrary\bin\Debug\EF5CodeFirstClassLibrary.dll");
                //var efASMPath = @"C:\Users\rajneesh.kumar\source\repos\MasterSolution\EF5CodeFirstClassLibraryReflection\bin\Debug\EntityFramework.dll";

                var projASM = Assembly.LoadFrom(@"C:\Publish\CodeFirstSample\bin\EF5CodeFirstClassLibrary.dll");
                var efASMPath = @"C:\Publish\CodeFirstSample\bin\EntityFramework.dll";

                Type type = projASM.GetType("EF5CodeFirstClassLibrary.SampleDBContext");

                EntityCodeFirstProcess.RunProcess(type, efASMPath);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                EntityCodeFirstProcess.RemoveConnectionString(appConfigPath, nodes);
            }
        }
       
        private static string GetProjectEdmx()
        {
            Console.WriteLine("Enter 4 for EF4");
            Console.WriteLine("Enter 5 for EF5");
            Console.WriteLine("Enter 6 for EF6");
            var assemblyPath = Console.ReadLine();

            string edmx = string.Empty;
            try
            {
                bool isIntiger = int.TryParse(assemblyPath, out int integervalue);

                Type DbMigrationsConfigurationType = null;

                Type DbMigratortype = null;

                Type type = null;

                Assembly entityAssembly = null;

                Assembly projectAssembly = null;

                string EDMXFILENAME = string.Empty;

                string[] DbContextAssembly = { @"C:\Users\rajneesh.kumar\source\repos\MasterSolution\EF5CodeFirstClassLibrary\bin\Debug\EF5CodeFirstClassLibrary.dll", @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\EF6ClassLibraryCodeFirst\bin\Debug\EF6ClassLibraryCodeFirst.dll", @"C:\Users\rajneesh.kumar\source\repos\EF4CodeFirst\EF4CodeFirst\bin\Debug\EF4CodeFirst.dll" };
                string[] EFAssembly = { @"C:\Users\rajneesh.kumar\source\repos\MasterSolution\EF5CodeFirstClassLibraryReflection\bin\Debug\EntityFramework.dll", @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\EF6ClassLibraryCodeFirst\bin\Debug\EntityFramework.dll", @"C:\Users\rajneesh.kumar\source\repos\EF4CodeFirst\EF4CodeFirst\bin\Debug\EntityFramework.dll" };

                Console.WriteLine("Generating...");

                if (integervalue == 4)
                {
                    /*Load project dll*/
                    projectAssembly = Assembly.LoadFrom(DbContextAssembly[2]);
                    type = projectAssembly.GetType("EF4CodeFirst.SampleDBContext");
                    /*Load Entity Framework*/
                    entityAssembly = Assembly.LoadFrom(EFAssembly[2]);
                    EDMXFILENAME = "EF4_EDMX.xml";
                }
                if (integervalue == 5)
                {   /*Load project dll*/
                    projectAssembly = Assembly.LoadFrom(DbContextAssembly[0]);
                    type = projectAssembly.GetType("EF5CodeFirstClassLibrary.SampleDBContext");
                    //    /*Load Entity Framework*/
                    entityAssembly = Assembly.LoadFrom(EFAssembly[2]);
                    EDMXFILENAME = "EF5_EDMX.xml";
                }
                if (integervalue == 6)
                {
                    /*Load project dll*/
                    projectAssembly = Assembly.LoadFrom(DbContextAssembly[1]);
                    type = projectAssembly.GetType("EF6ClassLibraryCodeFirst.SampleDBContext");
                    var tbc = type.BaseType.GetConstructors();
                    /*Load Entity Framework*/
                    entityAssembly = Assembly.LoadFrom(EFAssembly[1]);
                    EDMXFILENAME = "EF6_EDMX.xml";
                }


                DbMigrationsConfigurationType = entityAssembly.GetType("System.Data.Entity.Migrations.DbMigrationsConfiguration`1");

                DbMigratortype = entityAssembly.GetType("System.Data.Entity.Migrations.DbMigrator");



                string connectionstringname = GeConnectionStringNameSpecifedinContextClass(type);

                AppendXml(connectionstringname, @"C:\Users\rajneesh.kumar\source\repos\MasterSolution\EF5CodeFirstRefelection\bin\Debug\EF5CodeFirstRefelection.exe.config");

                // Type type2 = projectAssembly.GetType("EF5CodeFirstClassLibrary.SampleDBContext");
                Type constructedClass = DbMigrationsConfigurationType.MakeGenericType(type);

                dynamic config = Activator.CreateInstance(constructedClass);

                config.AutomaticMigrationsEnabled = true;

                var migrator = Activator.CreateInstance(DbMigratortype, new object[] { config });

                var _currenModelProp = migrator.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Single(m => m.Name == "_currentModel");

                var _currenModelValueXDOC = (XDocument)_currenModelProp.GetValue(migrator);

                var edmxFile = CreateMigrationDirectory(@"C:\EFCodeFirst") + EDMXFILENAME;

                File.WriteAllText(edmxFile, _currenModelValueXDOC.ToString());

                Console.WriteLine("{0} generated at {1}.", EDMXFILENAME, edmxFile);

                edmx = _currenModelValueXDOC.ToString();

            }
            catch (Exception ex)
            {
                Console.Write("Error occured: {0}", ex);
            }
            return edmx;
        }
        private static string GeConnectionStringNameSpecifedinContextClass(Type type)
        {
            var constructor = type.GetConstructors().FirstOrDefault(c => c.GetParameters().All(p => p.IsOptional));

            var parameters = constructor.GetParameters().Select(p => Type.Missing).ToArray();

            var instance = constructor.Invoke(BindingFlags.OptionalParamBinding | BindingFlags.InvokeMethod | BindingFlags.CreateInstance | BindingFlags.Instance, null, parameters, CultureInfo.InvariantCulture);

            var InternalContextProp = instance.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(p => p.Name == "InternalContext");

            var InternalContextPropValue = InternalContextProp.GetValue(instance, null);

            var _internalConnectionProp = InternalContextPropValue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_internalConnection");

            var _internalConnectionPropValue = _internalConnectionProp.GetValue(InternalContextPropValue);

            var _nameOrConnectionStringProp = _internalConnectionPropValue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_nameOrConnectionString");

            var _nameOrConnectionStringPropValue = _nameOrConnectionStringProp.GetValue(_internalConnectionPropValue);

            //var connectionStringName = InternalContextPropValue.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Single(p => p.Name == "ConnectionStringName").GetValue(InternalContextPropValue);

            return _nameOrConnectionStringPropValue.ToString();
        }
        private static string InvokeInterfaceClassConstructor(Type type)
        {
            var constructor = type.GetConstructors().FirstOrDefault(c => c.GetParameters().All(p => p.IsOptional));

            var parameters = constructor.GetParameters().Select(p => Type.Missing).ToArray();

            var instance = constructor.Invoke(BindingFlags.OptionalParamBinding | BindingFlags.InvokeMethod | BindingFlags.CreateInstance | BindingFlags.Instance, null, parameters, CultureInfo.InvariantCulture);

            var InternalContextProp = instance.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(p => p.Name == "InternalContext");

            var InternalContextPropValue = InternalContextProp.GetValue(instance, null);

            var _internalConnectionProp = InternalContextPropValue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_internalConnection");

            var _internalConnectionPropValue = _internalConnectionProp.GetValue(InternalContextPropValue);

            var _nameOrConnectionStringProp = _internalConnectionPropValue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_nameOrConnectionString");

            var _nameOrConnectionStringPropValue = _nameOrConnectionStringProp.GetValue(_internalConnectionPropValue);

            //var connectionStringName = InternalContextPropValue.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Single(p => p.Name == "ConnectionStringName").GetValue(InternalContextPropValue);

            return _nameOrConnectionStringPropValue.ToString();
        }
        static string CreateMigrationDirectory(string path)
        {
            string migrationFilePath = path + "\\Migrations\\";
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(migrationFilePath);
                }
                catch (Exception ex)
                {
                    // handle them here
                }
            }
            return migrationFilePath;
        }
        public static void AppendXml(string connnectionStringName, string AppConfigPath)
        {
            string name = string.Empty;
            if (connnectionStringName.Contains("="))
            {
                string[] conname = connnectionStringName.Split('=');
                name = conname[1];
            }
            else
            {
                name = connnectionStringName;
            }
            XmlDocument doc = new XmlDocument();

            doc.Load(AppConfigPath);

            XmlNode parentNode = doc.SelectSingleNode("//configuration//connectionStrings");

            //Create a new node.
            XmlElement childNode = doc.CreateElement("add");

            childNode.SetAttribute("name", name);

            childNode.SetAttribute("connectionString", "Data Source=SEZ3CROSSCODE\\SQLEXPRESS;Initial Catalog=SampleDB2;Integrated Security=True");

            childNode.SetAttribute("providerName", "System.Data.SqlClient");

            parentNode.InsertBefore(childNode, parentNode.FirstChild);

            doc.Save(AppConfigPath);

            Console.WriteLine("Log file location updated.");

        }



    }
}
