﻿using CrossCode.BLL.Domain.DBUsageStructure1;
using CrossCode.BLL.Domain1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace EF5CodeFirstRefelection
{  /// <summary>
   /// This class handles xml related activity
   /// </summary>
    public class XMLHelper
    {
        /// <summary>
        /// Get the Namespace
        /// </summary>
        /// <param name="csdlData">all the content of csdl file</param>
        /// <param name="entityContainer">Entity container inside the edmx file</param>
        /// <returns>Namespace of entity container class</returns>
        public static string GetNamespace(string csdlData, out string entityContainer)
        {
            entityContainer = string.Empty;

            Dictionary<string, string> lstORMMapping = new Dictionary<string, string>();
            string nsmName = string.Empty;

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(csdlData);
                nsmName = doc.ChildNodes.Item(1).Attributes["Namespace"].Value;

                if (doc.ChildNodes.Item(1).HasChildNodes)
                {
                    entityContainer = doc.ChildNodes.Item(1).ChildNodes.Item(0).Attributes["Name"].Value;
                }
            }
            catch (Exception ex)
            {
                //AppLog.Error("GetNamespace : Following error occured - ", ex);
            }

            return nsmName;
        }
        /// <summary>
        /// Get the Namespace
        /// </summary>
        /// <param name="csdlData">all the content of csdl file</param>
        /// <param name="entityContainer">Entity container inside the edmx file</param>
        /// <returns>Namespace of entity container class</returns>
        public static void GetNamespace(string csdlData, out string entityContainer, out string nameSpacevalue)
        {
            entityContainer = string.Empty;
            nameSpacevalue = string.Empty;

            Dictionary<string, string> lstORMMapping = new Dictionary<string, string>();

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(csdlData);
                var Schema = doc.GetElementsByTagName("Schema");
                nameSpacevalue = Schema[0].Attributes["Namespace"].Value;
                if (Schema[0].HasChildNodes)
                {
                    entityContainer = doc.GetElementsByTagName("EntityContainer")[0].Attributes["Name"].Value;
                }
            }
            catch (Exception ex)
            {
                //AppLog.Error("GetNamespace : Following error occured - ", ex);
            }
        }
        /// <summary>
        /// Get all the table mapping from ssdl file
        /// </summary>
        /// <param name="ssdl">Content of ssdl file</param>
        /// <returns>Key with table and columns are values in dictionary</returns>
        public static Dictionary<string, string> GetTablesMapping(string ssdl)
        {
            Dictionary<string, string> lstORMMapping = new Dictionary<string, string>();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ssdl);

            var entitySet = doc.ChildNodes.Item(1);
            XmlNode entityContainer = null;

            foreach (XmlNode entContainer in doc.ChildNodes.Item(1))
            {
                if (entContainer.Name == "EntityContainer")
                {
                    entityContainer = entContainer;
                    break;
                }
            }

            foreach (XmlNode xn in entityContainer.ChildNodes)
            {
                if (xn.Name == "EntitySet" && xn.Attributes["store:Type"].Value == "Tables")
                {
                    string databaseName = xn.Attributes["Name"].Value;
                    string entityClassName = xn.Attributes["EntityType"].Value;

                    lstORMMapping.Add(databaseName, entityClassName);
                }
            }

            return lstORMMapping;
        }
        /// <summary>
        /// Get all the table mapping from ssdl file
        /// </summary>
        /// <param name="ssdl">Content of ssdl file</param>
        /// <returns>Key with table and columns are values in dictionary</returns>
        public static Dictionary<string, string> GetTablesMapping(string ssdl, string temp = null)
        {
            Dictionary<string, string> lstORMMapping = new Dictionary<string, string>();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ssdl);

                XmlNode entityContainerNode = null;

                var entityContainer = doc.GetElementsByTagName("EntityContainer");

                if (entityContainer.Count > 0)
                {
                    if (entityContainer[0].HasChildNodes)
                    {
                        entityContainerNode = entityContainer[0];
                    }
                }
                foreach (XmlNode xn in entityContainerNode.ChildNodes)
                {
                    if (xn.Name == "EntitySet")
                    {
                        string entityClassName = xn.Attributes["Name"].Value;
                        string databaseTableName = xn.Attributes["Table"].Value;
                        lstORMMapping.Add(entityClassName, databaseTableName);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return lstORMMapping;
        }

        public static Dictionary<Dictionary<string, string>, Dictionary<string, string>> GetDBNORMMapping(string msl, Dictionary<string, string> dbdataMapped, string option = null)
        {
            Dictionary<Dictionary<string, string>, Dictionary<string, string>> lstORMMapping = new Dictionary<Dictionary<string, string>, Dictionary<string, string>>();

            Dictionary<Dictionary<string, string>, Dictionary<string, string>> lstORMAssociationSetMapping = new Dictionary<Dictionary<string, string>, Dictionary<string, string>>();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(msl);

            var entitySetMapping = doc.GetElementsByTagName("EntityContainerMapping");

            foreach (XmlNode xn in entitySetMapping.Item(0).ChildNodes)
            {
                if (xn.Name == "EntitySetMapping")
                {
                    string clsName = xn.Attributes["Name"].Value;

                    Dictionary<string, string> dbclsMapping = new Dictionary<string, string>();

                    string mappedClass = xn.FirstChild.Attributes["TypeName"].Value;

                    var columnNameDB = xn.ChildNodes.Item(0).ChildNodes.Item(0).Attributes["StoreEntitySet"].Value;

                    if (mappedClass.Contains("IsTypeOf"))
                    {
                        string tempCls = mappedClass.Substring(9, mappedClass.Length - 10);
                        mappedClass = tempCls.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries)[1];
                    }

                    if (option == "EFCF")
                    {
                        dbclsMapping.Add(dbdataMapped[columnNameDB], columnNameDB);
                    }
                    else
                    {
                        dbclsMapping.Add(columnNameDB, mappedClass);
                    }

                    if (dbdataMapped.Keys.Contains(columnNameDB))
                    {
                        var scalerProperty = xn.ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes;

                        if (scalerProperty.Count > 0)
                        {
                            Dictionary<string, string> columnList = new Dictionary<string, string>();

                            foreach (XmlNode column in scalerProperty)
                            {
                                string columnName = string.Empty;
                                string columnProperty = string.Empty;
                                if (column.Name == "ScalarProperty")
                                {
                                    columnName = column.Attributes["ColumnName"].Value;
                                    columnProperty = column.Attributes["Name"].Value;
                                    columnList.Add(columnName, columnProperty);
                                }
                                else if (column.Name == "ComplexProperty")
                                {
                                    foreach (XmlNode cpColumn in column)
                                    {
                                        columnName = cpColumn.Attributes["ColumnName"].Value;
                                        columnProperty = cpColumn.Attributes["Name"].Value;
                                        columnList.Add(columnName, columnProperty);
                                    }
                                }
                            }

                            lstORMMapping.Add(dbclsMapping, columnList);
                        }
                    }
                }
                else if (xn.Name == "AssociationSetMapping")
                {
                    Dictionary<string, string> dbclsMapping = new Dictionary<string, string>();

                    string colName = xn.Attributes["StoreEntitySet"].Value;

                    string mappedClass = xn.Attributes["TypeName"].Value;

                    dbclsMapping.Add(colName, mappedClass);

                    if (dbdataMapped.Keys.Contains(colName))
                    {
                        if (xn.HasChildNodes)
                        {
                            Dictionary<string, string> columnList = new Dictionary<string, string>();

                            foreach (XmlNode child in xn.ChildNodes)
                            {
                                if (child.FirstChild != null)
                                {
                                    string mappedProp = child.FirstChild.Attributes["Name"].Value;
                                    string columnName = child.FirstChild.Attributes["ColumnName"].Value;
                                    columnList.Add(columnName, mappedProp);
                                }
                            }
                            lstORMAssociationSetMapping.Add(dbclsMapping, columnList);
                        }
                    }
                }
            }

            //Merg Assocition properties
            foreach (var ent in lstORMMapping)
            {
                foreach (var map in ent.Key)
                {
                    foreach (var ast in lstORMAssociationSetMapping)
                    {
                        foreach (var asstkey in ast.Key)
                        {
                            if (asstkey.Key == map.Value)
                            {
                                foreach (var set in ast.Value)
                                {
                                    if (!ent.Value.ContainsKey(set.Key))
                                        ent.Value.Add(set.Key, set.Value);
                                }
                            }
                        }
                    }
                }
            }
            return lstORMMapping;
        }

        /// <summary>
        /// Actual mapped table with column mapped in Code application
        /// </summary>
        /// <param name="msl">content of msl file</param>
        /// <param name="dbdataMapped">table mapped data</param>
        /// <returns>All the actual mapped database with code mapping</returns>

        public static Dictionary<Dictionary<string, string>, Dictionary<string, string>> GetDBNORMMapping(string msl, Dictionary<string, string> dbdataMapped)
        {
            Dictionary<Dictionary<string, string>, Dictionary<string, string>> lstORMMapping = new Dictionary<Dictionary<string, string>, Dictionary<string, string>>();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(msl);
            var entitySet = doc.ChildNodes.Item(1).FirstChild;

            foreach (XmlNode xn in entitySet.ChildNodes)
            {
                if (xn.Name == "EntitySetMapping")
                {
                    string clsName = xn.Attributes["Name"].Value;
                    Dictionary<string, string> dbclsMapping = new Dictionary<string, string>();

                    string mappedClass = xn.FirstChild.Attributes["TypeName"].Value;
                    var columnNameDB = xn.ChildNodes.Item(0).ChildNodes.Item(0).Attributes["StoreEntitySet"].Value;

                    if (mappedClass.Contains("IsTypeOf"))
                    {
                        string tempCls = mappedClass.Substring(9, mappedClass.Length - 10);
                        mappedClass = tempCls.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries)[1];
                    }

                    dbclsMapping.Add(columnNameDB, mappedClass);
                    if (dbdataMapped.Keys.Contains(columnNameDB))
                    {
                        var scalerProperty = xn.ChildNodes.Item(0).ChildNodes.Item(0).ChildNodes;
                        if (scalerProperty.Count > 0)
                        {
                            Dictionary<string, string> columnList = new Dictionary<string, string>();

                            foreach (XmlNode column in scalerProperty)
                            {
                                string columnName = column.Attributes["ColumnName"].Value;
                                string columnProperty = column.Attributes["Name"].Value;

                                columnList.Add(columnName, columnProperty);
                            }

                            lstORMMapping.Add(dbclsMapping, columnList);
                        }
                    }
                }
                else if (xn.Name == "AssociationSetMapping")
                {
                    Dictionary<string, string> dbclsMapping = new Dictionary<string, string>();

                    string colName = xn.Attributes["StoreEntitySet"].Value;
                    string mappedClass = xn.Attributes["TypeName"].Value;

                    dbclsMapping.Add(colName, mappedClass);

                    if (dbdataMapped.Keys.Contains(colName))
                    {
                        if (xn.HasChildNodes)
                        {
                            Dictionary<string, string> columnList = new Dictionary<string, string>();

                            foreach (XmlNode child in xn.ChildNodes)
                            {
                                string mappedProp = child.FirstChild.Attributes["Name"].Value;
                                string columnName = child.FirstChild.Attributes["ColumnName"].Value;

                                columnList.Add(columnName, mappedProp);
                            }

                            lstORMMapping.Add(dbclsMapping, columnList);
                        }
                    }
                }
            }

            return lstORMMapping;
        }
        /// <summary>
        /// Map in DomainmodelStructure to pass dependency module data structure
        /// </summary>
        /// <param name="mapList">table and column mapping list</param>
        /// <param name="dbusageStruct">Databaser usage structure</param>
        public static void Map(Dictionary<Dictionary<string, string>, Dictionary<string, string>> mapList, DBMappingDetails2 dbusageStruct, Type type)
        {

            dbusageStruct.GenericORMmapDetails = new List<IDBMapItem>();
            dbusageStruct.ORMmapDetails = new List<DBMapItem>();

            var propInfo = type.GetProperties()
                       .Where(f => f.PropertyType.IsGenericType == true && f.PropertyType.Name.Contains("DbSet`1")).Select(s => s.PropertyType.GetGenericArguments()[0]).ToArray();

            var dbSetPropInfo = propInfo.Select(s => new
            {
                propertyAssembly = s.Assembly.GetName().Name,
                PropertyNamespace = s.Namespace,
                PropertyClass = s.Name,
            }).ToList();


            foreach (var item in mapList)
            {
                string entityClassName = string.Empty;
                string tblName = string.Empty;
                Dictionary<string, string> columnPropertyMappedWithClass = new Dictionary<string, string>();

                foreach (var databaseame in item.Key)
                {
                    entityClassName = databaseame.Value.StartsWith(dbusageStruct.NameSpaceName + ".")
                                                   ? databaseame.Value.Substring(dbusageStruct.NameSpaceName.Count() + 1) : databaseame.Value;
                    tblName = databaseame.Key;
                }

                IDBMapItem lst = new DBMapItem2();
                lst.CollectionOFMappedColumnProperties = item.Value;

                dbusageStruct.GenericORMmapDetails.Add(new DBMapItem2
                {
                    TblNameKey = tblName,
                    MappedClass = entityClassName,
                    MappedNamespace = "",
                    MappedAssembly = "",
                    CollectionOFMappedColumnProperties = lst.CollectionOFMappedColumnProperties,
                    PropertiesTypes = null
                });


            }

           

            foreach (var gnrcORM in dbusageStruct.GenericORMmapDetails)
            {

                var ptype = propInfo.Single(d => d.Name == gnrcORM.MappedClass);

                string entityPropName = string.Empty;

                var typeBase = ptype.GetBaseClassesAndInterfaces().ToList();

                Dictionary<string, PropertyType> dictionary = new Dictionary<string, PropertyType>();

                if (typeBase.Count > 0)
                {
                    foreach (var btyp in typeBase)
                    {
                        var basePropinfo = TypeFinder.GetDeclaredProperties(btyp);

                        var basepropertyType = new PropertyType();

                        basepropertyType.MappedClass = btyp.Name;
                        basepropertyType.MappedNamespace = btyp.Namespace;
                        basepropertyType.MappedAssembly = btyp.Assembly.GetName().Name;
                        basepropertyType.CollectionOFMappedColumnProperties = new Dictionary<string, string>();
                        

                        foreach (var bp in basePropinfo)
                        {
                            var baseKeyValue = gnrcORM.CollectionOFMappedColumnProperties.Where(s => s.Value == bp.Name).Select(o => o).FirstOrDefault();

                            //Check for complex Type
                            if (baseKeyValue.Key == null && baseKeyValue.Value == null)
                            {
                                var complexProperty = TypeFinder.GetDeclaredProperties(bp.PropertyType);
                                var complexpropertyType = new PropertyType();
                                complexpropertyType.MappedClass = bp.PropertyType.Name;
                                complexpropertyType.MappedNamespace = bp.PropertyType.Namespace;
                                complexpropertyType.MappedAssembly = bp.PropertyType.Assembly.GetName().Name;
                                complexpropertyType.CollectionOFMappedColumnProperties = new Dictionary<string, string>();
                                
                                Dictionary<string, PropertyType> Complexdictionary = new Dictionary<string, PropertyType>();

                                foreach (var cp in complexProperty)
                                {
                                    var complexTypeKeyValue = gnrcORM.CollectionOFMappedColumnProperties.Where(s => s.Value == cp.Name).Select(o => o).FirstOrDefault();
                                    complexpropertyType.CollectionOFMappedColumnProperties.Add(complexTypeKeyValue.Key, complexTypeKeyValue.Value);
                                    dictionary.Add(complexTypeKeyValue.Key, complexpropertyType);
                                }
                                ((DBMapItem2)gnrcORM).PropertiesTypes = dictionary;
                            }
                            else
                            {
                                basepropertyType.CollectionOFMappedColumnProperties.Add(baseKeyValue.Key, baseKeyValue.Value);
                                dictionary.Add(baseKeyValue.Key, basepropertyType);
                            }
                        }
                        ((DBMapItem2)gnrcORM).PropertiesTypes = dictionary;
                    }
                }
            }

            foreach (var info in dbSetPropInfo)
            {
                ((DBMapItem2)dbusageStruct.GenericORMmapDetails.Find(f => f.MappedClass == info.PropertyClass)).MappedNamespace = info.PropertyNamespace;
                ((DBMapItem2)dbusageStruct.GenericORMmapDetails.Find(f => f.MappedClass == info.PropertyClass)).MappedAssembly = info.propertyAssembly;
            }

        }
    }
}