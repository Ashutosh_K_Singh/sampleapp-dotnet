﻿using EF5CodeFirstClassLibrary;
using EF5CodeFirstClassLibraryTeacher;
using System.Data.Entity;

namespace EFCodeFirstContext
{
    public class SampleDBContext : DbContext
    {

        public SampleDBContext() : base("name=SampleDb-EF5CodeFirst")
        {
           
            Database.SetInitializer(new SampleDBInitializer());
        }

        public DbSet<Employee> Employee { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<Teacher> Teacher { get; set; }
        public DbSet<Student> Student { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Blog> Blogs { get; set; }
       
    }
}
