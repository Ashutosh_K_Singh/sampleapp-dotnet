﻿namespace MappingWindowsForms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog_dll = new System.Windows.Forms.OpenFileDialog();
            this.textBox_projectDLL = new System.Windows.Forms.TextBox();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.OK_btn = new System.Windows.Forms.Button();
            this.label_result = new System.Windows.Forms.TextBox();
            this.textBox_EFDll = new System.Windows.Forms.TextBox();
            this.textBox_edmx = new System.Windows.Forms.TextBox();
            this.button_EFDLL = new System.Windows.Forms.Button();
            this.button_EDMX = new System.Windows.Forms.Button();
            this.textBox_projectConfig = new System.Windows.Forms.TextBox();
            this.button_config = new System.Windows.Forms.Button();
            this.label_projectconfig = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // openFileDialog_dll
            // 
            this.openFileDialog_dll.FileName = "openFileDialog_dll";
            // 
            // textBox_projectDLL
            // 
            this.textBox_projectDLL.Location = new System.Drawing.Point(12, 32);
            this.textBox_projectDLL.Name = "textBox_projectDLL";
            this.textBox_projectDLL.Size = new System.Drawing.Size(381, 20);
            this.textBox_projectDLL.TabIndex = 0;
            // 
            // BrowseButton
            // 
            this.BrowseButton.Location = new System.Drawing.Point(408, 22);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(92, 39);
            this.BrowseButton.TabIndex = 1;
            this.BrowseButton.Text = "Project DLL";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
            // 
            // OK_btn
            // 
            this.OK_btn.Location = new System.Drawing.Point(524, 22);
            this.OK_btn.Name = "OK_btn";
            this.OK_btn.Size = new System.Drawing.Size(76, 39);
            this.OK_btn.TabIndex = 4;
            this.OK_btn.Text = "OK";
            this.OK_btn.UseVisualStyleBackColor = true;
            this.OK_btn.Click += new System.EventHandler(this.OK_btn_Click);
            // 
            // label_result
            // 
            this.label_result.Location = new System.Drawing.Point(12, 117);
            this.label_result.Multiline = true;
            this.label_result.Name = "label_result";
            this.label_result.Size = new System.Drawing.Size(588, 387);
            this.label_result.TabIndex = 6;
            // 
            // textBox_EFDll
            // 
            this.textBox_EFDll.Location = new System.Drawing.Point(719, 32);
            this.textBox_EFDll.Name = "textBox_EFDll";
            this.textBox_EFDll.Size = new System.Drawing.Size(381, 20);
            this.textBox_EFDll.TabIndex = 8;
            // 
            // textBox_edmx
            // 
            this.textBox_edmx.Location = new System.Drawing.Point(689, 117);
            this.textBox_edmx.Multiline = true;
            this.textBox_edmx.Name = "textBox_edmx";
            this.textBox_edmx.Size = new System.Drawing.Size(588, 387);
            this.textBox_edmx.TabIndex = 9;
            // 
            // button_EFDLL
            // 
            this.button_EFDLL.Location = new System.Drawing.Point(1118, 22);
            this.button_EFDLL.Name = "button_EFDLL";
            this.button_EFDLL.Size = new System.Drawing.Size(109, 40);
            this.button_EFDLL.TabIndex = 11;
            this.button_EFDLL.Text = "EntityFramework DLL";
            this.button_EFDLL.UseVisualStyleBackColor = true;
            this.button_EFDLL.Click += new System.EventHandler(this.button_EFDLL_Click);
            // 
            // button_EDMX
            // 
            this.button_EDMX.Location = new System.Drawing.Point(1258, 23);
            this.button_EDMX.Name = "button_EDMX";
            this.button_EDMX.Size = new System.Drawing.Size(97, 39);
            this.button_EDMX.TabIndex = 12;
            this.button_EDMX.Text = "Run For EDMX";
            this.button_EDMX.UseVisualStyleBackColor = true;
            this.button_EDMX.Click += new System.EventHandler(this.button_EDMX_Click);
            // 
            // textBox_projectConfig
            // 
            this.textBox_projectConfig.Location = new System.Drawing.Point(719, 91);
            this.textBox_projectConfig.Name = "textBox_projectConfig";
            this.textBox_projectConfig.Size = new System.Drawing.Size(381, 20);
            this.textBox_projectConfig.TabIndex = 13;
            // 
            // button_config
            // 
            this.button_config.Location = new System.Drawing.Point(1118, 71);
            this.button_config.Name = "button_config";
            this.button_config.Size = new System.Drawing.Size(109, 40);
            this.button_config.TabIndex = 14;
            this.button_config.Text = "Add Config";
            this.button_config.UseVisualStyleBackColor = true;
            this.button_config.Click += new System.EventHandler(this.button_config_Click);
            // 
            // label_projectconfig
            // 
            this.label_projectconfig.AutoSize = true;
            this.label_projectconfig.Location = new System.Drawing.Point(716, 71);
            this.label_projectconfig.Name = "label_projectconfig";
            this.label_projectconfig.Size = new System.Drawing.Size(118, 13);
            this.label_projectconfig.TabIndex = 15;
            this.label_projectconfig.Text = "Add Project config path";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1396, 621);
            this.Controls.Add(this.label_projectconfig);
            this.Controls.Add(this.button_config);
            this.Controls.Add(this.textBox_projectConfig);
            this.Controls.Add(this.button_EDMX);
            this.Controls.Add(this.button_EFDLL);
            this.Controls.Add(this.textBox_edmx);
            this.Controls.Add(this.textBox_EFDll);
            this.Controls.Add(this.label_result);
            this.Controls.Add(this.OK_btn);
            this.Controls.Add(this.BrowseButton);
            this.Controls.Add(this.textBox_projectDLL);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog_dll;
        private System.Windows.Forms.TextBox textBox_projectDLL;
        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.Button OK_btn;
        private System.Windows.Forms.TextBox label_result;
        private System.Windows.Forms.TextBox textBox_EFDll;
        private System.Windows.Forms.TextBox textBox_edmx;
        private System.Windows.Forms.Button button_EFDLL;
        private System.Windows.Forms.Button button_EDMX;
        private System.Windows.Forms.TextBox textBox_projectConfig;
        private System.Windows.Forms.Button button_config;
        private System.Windows.Forms.Label label_projectconfig;
    }
}

