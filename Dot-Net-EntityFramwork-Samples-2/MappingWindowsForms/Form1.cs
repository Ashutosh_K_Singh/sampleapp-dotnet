﻿using CodeFirstClassLibrary;
using EF5CodeFirstRefelection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace MappingWindowsForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void OK_btn_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox_projectDLL.Text))
            {
                var dto = Test.ExtractDll(textBox_projectDLL.Text);
                label_result.Text = new JavaScriptSerializer().Serialize(dto);
            }

        }

        private void BrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog_dll = BrowseFile("dll");
            if (openFileDialog_dll.ShowDialog() == DialogResult.OK)
            {
                string projectDll = openFileDialog_dll.FileName;

                textBox_projectDLL.Text = projectDll;

                var filename = Path.GetFileName(projectDll);

                string Efdll = projectDll.Replace(filename, "EntityFramework.dll");

                textBox_EFDll.Text = Efdll;

            }
        }
        OpenFileDialog BrowseFile(string fileExtention)
        {
            string filterbyextn = string.Format("{0} files(*.{0}) | *.{0}", fileExtention);
            return new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse Dll",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = fileExtention,
                //Filter = "dll files (*.dll)|*.dll",
                Filter = filterbyextn,
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };
        }
        private void button_EFDLL_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog_dll = BrowseFile("dll");

            if (openFileDialog_dll.ShowDialog() == DialogResult.OK)
            {
                textBox_EFDll.Text = openFileDialog_dll.FileName;

            }
        }

        private void button_config_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog_dll = BrowseFile("config");

            if (openFileDialog_dll.ShowDialog() == DialogResult.OK)
            {
                textBox_projectConfig.Text = openFileDialog_dll.FileName;

            }
        }

        private void button_EDMX_Click(object sender, EventArgs e)
        {
            string projecAssemblypath = textBox_projectDLL.Text;
            string efASMPath = textBox_EFDll.Text;
            string projectConfigPath = textBox_projectConfig.Text;
            string appConfigPath = "";
            var xml = EntityCodeFirstProcess.RunEdmxProcess(appConfigPath, projectConfigPath, projecAssemblypath, efASMPath);
            textBox_edmx.Text = xml;
        }
    }
}
