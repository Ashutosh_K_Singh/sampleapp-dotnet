﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstParameterizedContext
{
    public class SampleDBContext : DbContext
    {
        private readonly ILogger _ilogger;

        public SampleDBContext(): base("name=SampleDb-EF6CodeFirstParameterized")
        {
        }

        public DbSet<Employee> Employee { get; set; }
        public DbSet<Student> Student { get; set; }


    }
}
