﻿using CodeFirstParameterizedContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstParameterizedContextConsoleApp
{
    public class ContextFactory : IDbContextFactory<SampleDBContext>
    {
        public SampleDBContext Create()
        {
            return new SampleDBContext(null, null);
        }
    }
}
