﻿using CodeFirstParameterizedContext;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CodeFirstParameterizedContextConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {



            //var name = GeConnectionStringNameSpecifedinContextClass(typeof(SampleDBContext));
            //string[] conname = name.Split('=');
            
            AppendXml("SampleDb-EF6CodeFirstParameterized", @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\CodeFirstParameterizedContextConsoleApp\bin\Debug\CodeFirstParameterizedContextConsoleApp.exe.config");
            var migrationConfig = new DbMigrationsConfiguration<SampleDBContext>();
            var migrator = new DbMigrator(migrationConfig);

        }
        private static string GeConnectionStringNameSpecifedinContextClass(Type type)
        {
            var constructor = type.GetConstructors().FirstOrDefault(c => c.GetParameters().All(p => p.IsOptional));

            var parameters = constructor.GetParameters().Select(p => Type.Missing).ToArray();

            var instance = constructor.Invoke(BindingFlags.OptionalParamBinding | BindingFlags.InvokeMethod | BindingFlags.CreateInstance | BindingFlags.Instance, null, parameters, CultureInfo.InvariantCulture);

            var InternalContextProp = instance.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(p => p.Name == "InternalContext");

            var InternalContextPropValue = InternalContextProp.GetValue(instance);

            var _internalConnectionProp = InternalContextPropValue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_internalConnection");

            var _internalConnectionPropValue = _internalConnectionProp.GetValue(InternalContextPropValue);

            var _nameOrConnectionStringProp = _internalConnectionPropValue.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Single(f => f.Name == "_nameOrConnectionString");

            var _nameOrConnectionStringPropValue = _nameOrConnectionStringProp.GetValue(_internalConnectionPropValue);

            /*Change value of property*/
            // _nameOrConnectionStringProp.SetValue(_internalConnectionPropValue, null)       

            //var connectionStringName = InternalContextPropValue.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Single(p => p.Name == "ConnectionStringName").GetValue(InternalContextPropValue);
            type = null;
            return _nameOrConnectionStringPropValue != null ? _nameOrConnectionStringPropValue.ToString() : null;


        }

        public static void AppendXml(string connnectionStringName, string AppConfigPath)
        {

            XmlDocument doc = new XmlDocument();

            doc.Load(AppConfigPath);

            XmlNode parentNode = doc.SelectSingleNode("//configuration//connectionStrings");

            //Create a new node.
            XmlElement childNode = doc.CreateElement("add");

            childNode.SetAttribute("name", connnectionStringName);

            childNode.SetAttribute("connectionString", "Data Source=SEZ3CROSSCODE\\SQLEXPRESS;Initial Catalog=SampleDB2;Integrated Security=True");
            childNode.SetAttribute("providerName", "System.Data.SqlClient");
            parentNode.InsertBefore(childNode, parentNode.FirstChild);

            doc.Save(AppConfigPath);

            Console.WriteLine("Log file location updated.");

        }
    }
}
