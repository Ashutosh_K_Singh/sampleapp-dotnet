﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjectionClassLibrary
{
    public class Hello : IHello
    {
        public string SayHello(string str)
        {
            Console.WriteLine("Hello");
            return str;
        }
    }
}
