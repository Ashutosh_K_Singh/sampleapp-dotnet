﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjectionClassLibrary
{
    public interface IHello
    {
        string SayHello(string str);
    }
}
