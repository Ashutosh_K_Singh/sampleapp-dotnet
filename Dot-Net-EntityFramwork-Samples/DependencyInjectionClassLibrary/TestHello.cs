﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjectionClassLibrary
{
    public class TestHello
    {
        private IHello _ihello;
        public TestHello(IHello ihello)
        {
            this._ihello = ihello;
        }
        public void TestMethod(string str)
        {
            _ihello.SayHello(str);
        }
    }
}
