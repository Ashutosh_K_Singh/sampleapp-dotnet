﻿using Mono.Cecil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disambler
{
    class Program
    {
        static string assemblyPath = @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\EF6ClassLibraryFluentAPI\bin\Debug\EF6ClassLibraryFluentAPI.dll";
        static void Main(string[] args)
        {
            LoadAssembly();
            GetAssemblyContent();
            Console.ReadLine();
        }
        public static void LoadAssembly()
        {
            var assembly = AssemblyDefinition.ReadAssembly(assemblyPath);

            var item1=assembly.MainModule.Name.ToString();
            var item2=assembly.MainModule.Runtime.ToString();
            var item3=assembly.MainModule.FullyQualifiedName.ToString();
            var item4=assembly.MainModule.MetadataToken.ToString();
            var item5=assembly.MainModule.Architecture.ToString();
            //var item6=assembly.MainModule.EntryPoint.ToString();
            var item7=assembly.MainModule.Mvid.ToString();

        
           
        }

        public static void GetAssemblyContent()
        {
            var Asmbly = AssemblyDefinition.ReadAssembly(assemblyPath);
            //    IEnumerator enumerator = Asmbly.MainModule.Types.GetEnumerator();
            //while (enumerator.MoveNext())
            //{
            //    TypeDefinition td = (TypeDefinition)enumerator.Current;


            //    IEnumerator enumerator2 = td.Methods.GetEnumerator();
            //    while (enumerator2.MoveNext())
            //    {
            //        MethodDefinition method_definition = (MethodDefinition)enumerator2.Current;
            //        if (method_definition.IsConstructor)
            //        {
            //            //tn.Nodes.Add(method_definition.Name.ToString());
            //        }
            //        //tn.Nodes.Add(method_definition.Name.ToString());
            //    }
            //}

            var ty = Asmbly.MainModule.Types.Single(f => f.FullName == "EF6ClassLibraryFluentAPI.SampleDBContext");
            var mi = ty.Methods.Single(m => m.Name == ".ctor");
            var body = mi.Body.Instructions.ToList();
            foreach (var b in body) {
                Console.WriteLine("IL Value: {0}",b.Operand);

            }
        }
    }
}
