﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicAssembly
{
    public class MyContract : IMyContract
    {
        public void MethodA()
        {
            Console.WriteLine("MethodA from MyContract class");
        }
    }
}
