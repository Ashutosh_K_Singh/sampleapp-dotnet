﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DynamicAssembly
{
    class Program
    {

        static void Main(string[] args)
        {
            //CreateClassAtRuntime();
            CreateInstanceOfInterfaceContructorType();
            Console.ReadLine();
        }


        static void CreateInstanceOfInterfaceContructorType()
        {
            string _path = @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\DependencyInjectionClassLibrary\bin\Debug\DependencyInjectionClassLibrary.dll";

            var assembly = Assembly.LoadFrom(_path);

            Type[] types = assembly.GetTypes().Where(f => f.IsSealed == false).Select(s => s).ToArray();

            Type testHelloClass = assembly.GetType("DependencyInjectionClassLibrary.TestHello");

            Type interfaceHello = assembly.GetType("DependencyInjectionClassLibrary.IHello");

            DynamicClassBuilder classBuilder = new DynamicClassBuilder("MyClass");

            var myclass = classBuilder.CreateObject(new string[2] { "ID", "Name" }, new Type[2] { typeof(int), typeof(string) }, interfaceHello);
          
            object instance = Activator.CreateInstance(testHelloClass, myclass);

            Type TP = instance.GetType();
           
            MethodInfo mInfo = TP.GetMethod("TestMethod");

            var m = mInfo.Invoke(instance, new object[0]);





        }
        static void CreateClassAtRuntime()
        {
            DynamicClassBuilder classBuilder = new DynamicClassBuilder("MyClass");

            var myclass = classBuilder.CreateObject(new string[3] { "ID", "Name", "Address" }, new Type[3] { typeof(int), typeof(string), typeof(string) }, typeof(IMyContract));

            Type TP = myclass.GetType();

            //Get MyClass Properties
            foreach (PropertyInfo PI in TP.GetProperties())
            {
                Console.WriteLine("class Property availble:{0}", PI.Name);
            }
            //Get MyClass MethodInfo
            foreach (MethodInfo MI in TP.GetMethods())
            {
                Console.WriteLine("class Methods availble:{0}", MI.Name);

            }
            //Interface methods
            Type intrfc = TP.GetInterface("IMyContract");

            if (intrfc != null)
            {
                foreach (MethodInfo MI in intrfc.GetMethods())
                {
                    Console.WriteLine("Interface method name:{0} implimented in class", MI.Name);
                }

            }
        }

    }
}
