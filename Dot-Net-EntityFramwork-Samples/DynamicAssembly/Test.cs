﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicAssembly
{
    public class Test
    {
        private IMyContract imycontract;
        public Test(IMyContract _imyContract)
        {
            imycontract = _imyContract;
        }
       public void TestMethod()
        {
            imycontract.MethodA();
        }
    }
}
