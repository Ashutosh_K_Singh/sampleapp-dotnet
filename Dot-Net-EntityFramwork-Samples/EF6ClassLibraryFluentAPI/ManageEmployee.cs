﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6ClassLibraryFluentAPI
{
    public class ManageEmployee
    {
        public static IList<Employee> AddDemoEmployee()
        {
            IList<Employee> employee = new List<Employee>
            {
                new Employee() { EmployeeName = "Ravi", Department = "IT" },
                new Employee() { EmployeeName = "Tom", Department = "IT" },
                new Employee() { EmployeeName = "Rick", Department = "HR" },
                new Employee() { EmployeeName = "Sam", Department = "Cloud" }
            };

            return employee;
        }
        public static void AddEmployee(Employee employee)
        {
            using (var ctx = new SampleDBContext())
            {
                var entity = ctx.Entry(employee).State = System.Data.Entity.EntityState.Added;

                //ctx.Employee.Add(employee);
                ctx.SaveChanges();
            }
        }
        public static Employee EmployeeExist(Employee employee)
        {

            using (var ctx = new SampleDBContext())
            {
                //var id = from e
                //         in ctx.Employee
                //         where
                //            e.EmployeeName == employee.EmployeeName
                //            && e.Department == employee.Department
                //         select e;
                //return id.FirstOrDefault();
                return new Employee { Id = 8, EmployeeName = "TOM", Department = "HRD", IsActive = true };
            }

        }
        public static void DeleteEmployee(int employeeid)
        {
            using (var ctx = new SampleDBContext())
            using (var command = ctx.Database.Connection.CreateCommand())
            {
                command.CommandText = "DELETE FROM [FluentAPISampleDB].[Admin].[User] WHERE [Id]=" + employeeid;
                command.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        public static List<Employee> Get()
        {          
            List<Employee> emplist = new List<Employee>();
            using (var ctx = new SampleDBContext())
            using (var command = ctx.Database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT [Id],[EmployeeName],[Department],[IsActive] FROM [FluentAPISampleDB].[Admin].[User]";
                command.Connection.Open();
                using (var result = command.ExecuteReader())
                {
                    while (result.Read())
                    {
                        var emp = new Employee();
                        emp.Id = (int)result["Id"];
                        emp.EmployeeName = result["EmployeeName"].ToString();
                        emp.Department = result["Department"].ToString();
                        emp.IsActive = (bool)result["IsActive"];
                        emplist.Add(emp);
                    }

                }
                return emplist;

            }
        }
    }
}
