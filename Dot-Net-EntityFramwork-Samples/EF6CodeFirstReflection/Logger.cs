﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace EF6CodeFirstReflection
{
    public class Logger
    {
        public static void Log()
        {
            log4net.Config.BasicConfigurator.Configure();
            log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
            try
            {
                string str = String.Empty;
                string subStr = str.Substring(0, 4); //this line will create error as the string "str" is empty.  
            }
            catch (Exception ex)
            {
                log.Error("Error Message: " + ex.Message.ToString(), ex);
            }
        }
        /// <summary>
        /// Update log file location in app config
        /// </summary>
        /// <param name="AppConfigPath"></param>
        /// <param name="FileLocationAttributeValue"></param>
        public static void UpdateLogFileLocation(string AppConfigPath, string FileLocationAttributeValue)
        {

            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(AppConfigPath);

            XmlElement formData = (XmlElement)xmlDoc.SelectSingleNode("//log4net//appender//file");

            if (formData != null)
            {
                formData.SetAttribute("value", FileLocationAttributeValue); // Set to new value.
            }
            xmlDoc.Save(AppConfigPath);

            Console.WriteLine("Log file location updated.");
        }
        public static void AppendXml(string AppConfigPath)
        {

            XmlDocument doc = new XmlDocument();

            doc.Load(AppConfigPath);

            XmlElement xmlElement = (XmlElement)doc.SelectSingleNode("//connectionStrings//");

            XmlNode root = doc.DocumentElement;

            //Create a new node.
            XmlElement elem = doc.CreateElement("add");
            elem.SetAttribute("name", "SampleDb-EF6CodeFirstParameterized");
            elem.SetAttribute("connectionString", "value of connection string");          

            //Add the node to the document.
            root.AppendChild(elem);
            doc.Save(AppConfigPath);

            Console.WriteLine("Log file location updated.");
        }
    }
}
