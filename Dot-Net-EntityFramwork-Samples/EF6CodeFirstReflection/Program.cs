﻿using CrossCode.BLL.Domain.DBUsageStructure;
//using EF6ClassLibraryCodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
//using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Design;
using System.Data.Entity.Migrations.Infrastructure;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Resources;
using System.Text;
using System.Xml.Linq;

namespace EF6CodeFirstReflection
{
    public class Program
    {
        static string rootPath = @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\EF6CodeFirstReflection\UserCodeMapping";
        public static void Main(string[] args)
        {
            Console.WriteLine("Press 1 for CodeFirst Metadata");

            var assemblyPath = Console.ReadLine();

            bool isIntiger = int.TryParse(assemblyPath, out int integervalue);




            if (isIntiger)
            {
                switch (integervalue)
                {
                    case 1:
                        Console.WriteLine("Generating migrations...");

                        //GetAllMappingsEF6();

                        GetAllMappingsEF5();

                        var dbMappingDetails2 = CodeFirstRefelection.GetAttibutes(); ;

                        var entitiesInDbSet = CodeFirstRefelection.GetEntitiesFromAssembly();

                        var databaseInfo = CodeFirstRefelection.GetDatabaseDetail();

                        var filepath = @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\EF6CodeFirstReflection\bin\Debug\EF6CodeFirstReflection.exe.config";

                        Logger.UpdateLogFileLocation(filepath, "C:\\log1.txt");

                        break;
                    case 2:
                        GetManifestFromASM();
                        break;
                    default:

                        Console.WriteLine("Not found");

                        break;
                }
            }
            else
            {
                Console.WriteLine("Input type is not valid.");
            }
            Console.WriteLine("Completed.");

            Console.ReadLine();
        }

        /*
        public static string GetAllMigration()
        {

            var config = new DbMigrationsConfiguration<EF6ClassLibraryCodeFirst.SampleDBContext> { AutomaticMigrationsEnabled = true };

            var migrator = new DbMigrator(config);

            //Get code migration//
            var scaffolder = new MigrationScaffolder(migrator.Configuration);

            ScaffoldedMigration migration = scaffolder.Scaffold("codeMigration");

            var migrationFile = System.IO.Path.Combine(rootPath, migration.Directory, migration.MigrationId);

            var userCodeFile = migrationFile + ".cs";

            File.WriteAllText(userCodeFile, migration.UserCode);

            //Get Db script//
            var scriptor = new MigratorScriptingDecorator(migrator);

            string script = scriptor.ScriptUpdate(sourceMigration: null, targetMigration: null);

            var SqlScriptFile = migrationFile + ".sql";

            File.WriteAllText(SqlScriptFile, script);

            //Get Edmx Document//

            var _currenModelProp = migrator.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Single(m => m.Name == "_currentModel");

            var _currenModelValueXDOC = (XDocument)_currenModelProp.GetValue(migrator);

            var edmxFile = migrationFile + ".xml";


            File.WriteAllText(edmxFile, _currenModelValueXDOC.ToString());

            return script;
        }

        */
        static void GetAllMappingsEF6()
        {
            /*Looad code first*/

            var assemblyPath = @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\EF6ClassLibraryCodeFirst\bin\Debug";

            var assemblyName = string.Format(@"{0}\{1}.{2}", assemblyPath, "EF6ClassLibraryCodeFirst", "dll");

            var assembly = Assembly.LoadFrom(assemblyName);

            var type = assembly.GetType("EF6ClassLibraryCodeFirst.SampleDBContext");

            Type genericClass = typeof(DbMigrationsConfiguration<>);

            //MakeGenericType is badly named

            Type constructedClass = genericClass.MakeGenericType(type);

            object created = Activator.CreateInstance(constructedClass);

            ////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////

            var config = (DbMigrationsConfiguration)created;

            config.AutomaticMigrationsEnabled = true;

            var migrator = new DbMigrator(config);

            //Get code migration//
            var scaffolder = new MigrationScaffolder(migrator.Configuration);

            ScaffoldedMigration migration = scaffolder.Scaffold("codeMigration");

            var migrationFile = System.IO.Path.Combine(rootPath, migration.Directory, migration.MigrationId);

            var userCodeFile = migrationFile + ".cs";

            File.WriteAllText(userCodeFile, migration.UserCode);
            Console.WriteLine("Entity Migration generated.");
            //Get Db script//
            /*
            var scriptor = new MigratorScriptingDecorator(migrator);

            string script = scriptor.ScriptUpdate(sourceMigration: null, targetMigration: null);

            var SqlScriptFile = migrationFile + ".sql";

            File.WriteAllText(SqlScriptFile, script);
            Console.WriteLine("Sql Script  for migration generated.");
            */
            //Get Edmx Document//
            var _currenModelProp = migrator.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Single(m => m.Name == "_currentModel");

            var _currenModelValueXDOC = (XDocument)_currenModelProp.GetValue(migrator);

            var edmxFile = migrationFile + ".xml";

            File.WriteAllText(edmxFile, _currenModelValueXDOC.ToString());
            Console.WriteLine("Code First EDMX generated.");
        }
        static void GetAllMappingsEF5()
        {
            /*Looad code first*/

            var assemblyPath = @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\CodeFirstParameterizedContext\bin\Debug";

            var assemblyName = string.Format(@"{0}\{1}.{2}", assemblyPath, "CodeFirstParameterizedContext", "dll");

            var assembly = Assembly.LoadFrom(assemblyName);

            var type = assembly.GetType("CodeFirstParameterizedContext.SampleDBContext");

            Type genericClass = typeof(DbMigrationsConfiguration<>);

            //MakeGenericType is badly named
         

            Type constructedClass = genericClass.MakeGenericType(type);

          

            object created = Activator.CreateInstance(constructedClass, new object[] { });

            ////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////

            var config = (DbMigrationsConfiguration)created;

            config.AutomaticMigrationsEnabled = true;



            var migrator = new DbMigrator(config);

            //Get code migration//
            var scaffolder = new MigrationScaffolder(migrator.Configuration);

            ScaffoldedMigration migration = scaffolder.Scaffold("codeMigration");

            var migrationFile = System.IO.Path.Combine(rootPath, migration.Directory, migration.MigrationId);

            var userCodeFile = migrationFile + ".cs";

            File.WriteAllText(userCodeFile, migration.UserCode);
            Console.WriteLine("Entity Migration generated.");
            //Get Db script//
            /*
            var scriptor = new MigratorScriptingDecorator(migrator);

            string script = scriptor.ScriptUpdate(sourceMigration: null, targetMigration: null);

            var SqlScriptFile = migrationFile + ".sql";

            File.WriteAllText(SqlScriptFile, script);
            Console.WriteLine("Sql Script  for migration generated.");
            */
            //Get Edmx Document//
            var _currenModelProp = migrator.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Single(m => m.Name == "_currentModel");

            var _currenModelValueXDOC = (XDocument)_currenModelProp.GetValue(migrator);

            var edmxFile = migrationFile + ".xml";

            File.WriteAllText(edmxFile, _currenModelValueXDOC.ToString());
            Console.WriteLine("Code First EDMX generated.");
        }
        static void GetManifestFromASM()
        {
            var assemblyPath = @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\EF6ClassLibraryCodeFirst\bin\Debug";

            var assemblyName = string.Format(@"{0}\{1}.{2}", assemblyPath, "EF6ClassLibraryCodeFirst", "dll");

            var assembly = Assembly.LoadFrom(assemblyName);

            var type = assembly.GetType("EF6ClassLibraryCodeFirst.SampleDBContext");








            var manifest = assembly.GetManifestResourceNames();






            Stream strm0 = assembly.GetManifestResourceStream(manifest[1]);


            var _defaultReader = new ResourceReader(strm0);


            if (_defaultReader != null)
            {
                string strtype;

                byte[] bytedata;
                _defaultReader.GetResourceData("Target", out strtype, out bytedata);
                string result = System.Text.Encoding.UTF8.GetString(bytedata);

                var d = BitConverter.ToString(bytedata);




                ResourceWriter rw = new ResourceWriter(@".\TypeResources.resources");
                rw.AddResourceData("Integer1", "ResourceTypeCode.String", bytedata);
                //int n1 = 1032;
                //rw.AddResourceData("Integer1", "ResourceTypeCode.Int32", bytedata);
                //rw.AddResourceData("Integer1", "ResourceTypeCode.Int32", BitConverter.GetBytes(n1));
                //int n2 = 2064;
                //rw.AddResourceData("Integer2", "ResourceTypeCode.Int32", BitConverter.GetBytes(n2));
                rw.Generate();
                rw.Close();

                ResourceReader rr = new ResourceReader(@".\TypeResources.resources");
                System.Collections.IDictionaryEnumerator e = rr.GetEnumerator();
                while (e.MoveNext())
                    Console.WriteLine("{0}: {1}", e.Key, e.Value);
            }



            StreamReader reader0 = new StreamReader(strm0);

            string data0 = reader0.ReadToEnd();

            //Stream strm1 = assembly.GetManifestResourceStream(manifest[1]);
            //Stream strm2 = assembly.GetManifestResourceStream(manifest[2]);
            //Stream strm3 = assembly.GetManifestResourceStream(manifest[3]);
            //Stream strm4 = assembly.GetManifestResourceStream(manifest[4]);



            //StreamReader reader1 = new StreamReader(strm1);

            //string data1 = reader1.ReadToEnd();
            //StreamReader reader2 = new StreamReader(strm2);

            //string data2 = reader2.ReadToEnd();
            //StreamReader reader3 = new StreamReader(strm3);

            //string data3 = reader3.ReadToEnd();
            //StreamReader reader4 = new StreamReader(strm4);

            //string data4 = reader4.ReadToEnd();

            //long bytestreamMaxLength = strm.Length;
            //byte[] buffer = new byte[bytestreamMaxLength];
            //strm.Read(buffer, 0, (int)bytestreamMaxLength);
            //Assembly.Load(buffer);


            //EF6ClassLibraryCodeFirst.Migrations.Initial.resources

            var mfst1 = assembly.GetReferencedAssemblies();
            var mfMdl = assembly.ManifestModule;

            var mgrInital = mfMdl.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            foreach (var pi in mgrInital)
            {

                var value = pi.GetValue(mfMdl);

            }
        }

    }
}
