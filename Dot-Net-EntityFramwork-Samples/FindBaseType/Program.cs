﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FindBaseType
{
    class Program
    {
        static string asmPath = @"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\APIApp\bin\APIApp.dll";
        static void Main(string[] args)
        {
            var assembly = Assembly.LoadFrom(asmPath);

            Type[] types = assembly.GetTypes().Where(f => f.IsSealed == false).Select(s => s).ToArray();

            List<Type> derivedTypes = new List<Type>();

            for (int i = 0, count = types.Length; i < count; i++)
            {
                if (BaseFinder(types[i]))
                {
                    derivedTypes.Add(types[i]);

                    Console.WriteLine("{0} is subclass of ApiController", types[i].Name);
                }
            }
            Console.ReadLine();
        }

        public static bool BaseFinder(Type type, string BaseTypeName = "ApiController", string BaseTypeFullName = "System.Web.Http.ApiController")
        {
            if (type != null)
            {
                if (type.Name == BaseTypeName && type.FullName == BaseTypeFullName)
                {
                    return true;
                }
                else
                {
                    return BaseFinder(type.BaseType);
                }
            }
            return false;
        }
    }
}
