﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SerializeNdesirializeConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            SerializeGoodWork();
        }

        static void SerializeGoodWork()
        {
            GoodWork goodWork = new GoodWork();
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(@"C:\Users\rajneesh.kumar\source\repos\EF6-Code-First-Demo-master\SerializeNdesirializeConsoleApp\GoodWork.txt", FileMode.Create, FileAccess.Write);

            formatter.Serialize(stream, goodWork);
            stream.Close();
        }
    }
}
