﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppUsingFluentAPI.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public UserDetail UserDetail { get; set; }
        public UserInfo UserInfo { get; set; }

    }
    public class UserDetail
    {
        public int UserDetailId { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int UserId { get; set; }
    }
    public class UserInfo
    {
        public int UserInfoId { get; set; }
        public string GovId { get; set; }

    }
}