﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        string Message();

        [OperationContract]
        string SayHello(string value);
    }
}
