﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary
{
    [ServiceContract]
    public interface IServiceB : IService
    {
        [OperationContract]
        string CustomMessage(string value);

    }
}
