﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLibrary
{
    public class ServiceB : IServiceB
    {
        public string CustomMessage(string value)
        {
            return $"{value} This is a custome message from IServiceB.";
        }

        public string Message()
        {
            return "IService";
        }

        public string SayHello(string value)
        {
            return $"{value} IService";
        }
    }
}
