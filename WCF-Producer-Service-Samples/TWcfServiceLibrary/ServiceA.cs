﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TWcfServiceLibrary
{
    public class ServiceA : IService
    {
        public string Message()
        {
            return "ServiceA";
        }
        public string SayHello(string value)
        {
            return $"Hello {value} ServiceA";
        }
    }
}
