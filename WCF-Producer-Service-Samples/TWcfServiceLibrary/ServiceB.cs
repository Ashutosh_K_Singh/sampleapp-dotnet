﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TWcfServiceLibrary
{
    public class ServiceB : IService
    {
        public string Message()
        {
            return "ServiceB";
        }
        public string SayHello(string value)
        {
            return $"Hello {value} ServiceB";
        }
    }
}
