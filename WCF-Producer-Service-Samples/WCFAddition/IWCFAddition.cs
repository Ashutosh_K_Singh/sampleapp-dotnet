﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFAddition
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWCFAddition" in both code and config file together.
    [ServiceContract]
    public interface IWCFAddition
    {
        [OperationContract]
        void DoWork();


        [OperationContract]
        int SumOfTwoNumber(int num1, int num2);
    }
}
