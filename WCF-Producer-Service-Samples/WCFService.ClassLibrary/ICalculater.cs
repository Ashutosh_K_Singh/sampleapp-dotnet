﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCFService.ClassLibrary
{
    [ServiceContract]
    public interface ICalculater
    {
        [OperationContract]
        int Add(int x, int y);
        [OperationContract]
        int Multiply(int x, int y);
       
    }
}
