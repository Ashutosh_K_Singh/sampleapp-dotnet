﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace WCFService.Reflection
{
    public class ServiceModelDLLExtractor
    {
        public static Assembly LoadWCFAssembly(string assemblyPath)
        {
            try
            {
                var assembly = Assembly.LoadFrom(assemblyPath);

                return assembly;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<ServiceModelDTO> GetImplimentedInterfaceFromCLRType(Type clrType, string serviceFileName, List<ServiceModelDTO> listServicemodelDTO)
        {
            try
            {
                Type[] Interfaces = clrType.GetInterfaces();

                foreach (var IType in Interfaces)
                {
                    var dto = GetTypeInfo(IType);

                    dto.ServiceFileName = serviceFileName;

                    dto.ClassName = clrType.FullName;

                    listServicemodelDTO.Add(dto);

                    //Recusive for another interface which is implimented in current interface

                    return GetImplimentedInterfaceFromCLRType(IType, serviceFileName, listServicemodelDTO);
                }
                return listServicemodelDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static ServiceModelDTO GetTypeInfo(Type type)
        {
            try
            {
                ServiceModelDTO dto = new ServiceModelDTO();

                dto.Type = type;

                dto.TypeAssemblyName = type.AssemblyQualifiedName;

                dto.TypeNamespace = type.Namespace;

                dto.TypeName = type.Name;

                dto.TypeFullName = type.FullName;

                dto.IsInterface = type.IsInterface;

                ServiceContractAttribute serviceContractAttribute = GetServiceContractAttribute(type);

                dto.NameAttribute = serviceContractAttribute.NameAttribute;

                dto.NamespaceAttribute = serviceContractAttribute.NamespaceAttribute;

                dto.ConfigurationNameAttribute = serviceContractAttribute.ConfigurationNameAttribute;

                MethodInfo[] methodInfo = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (var minfo in methodInfo)
                {
                    string[] param = minfo.GetParameters().Select(p => String.Format("{0} {1}", p.ParameterType.Name, p.Name)).ToArray();

                    string signature = String.Format("{0} {1}({2})", minfo.ReturnType.Name, minfo.Name, String.Join(",", param));

                    string fullname = String.Format("{0}.{1}({2})", type.FullName, minfo.Name, String.Join(",", param));

                    OperationContractAttribute operationContract = GetOperationContractAttribute(minfo);

                    var method = new Method();

                    method.MethodName = minfo.Name;

                    method.MethodParmeters = param.ToList();

                    method.MethodSignature = signature;

                    method.MethodReturnType = minfo.ReturnType.Name;

                    method.MethodFullName = fullname;

                    method.NameAttribute = operationContract.NameAttribute;

                    method.ActionAttribute = operationContract.ActionAttribute;

                    method.ReplyActionAttribute = operationContract.ReplyActionAttribute;

                    dto.TypeMethods.Add(method);
                }

                return dto;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static OperationContractAttribute GetOperationContractAttribute(MethodInfo minfo)
        {
            try
            {
                OperationContractAttribute OperationContractAttribute = null;

                string[] attributeTypes = minfo.GetCustomAttributes(true).Select(x => x.GetType().Name).ToArray();

                if (Array.FindAll(attributeTypes, a => a.Equals("OperationContractAttribute")).Count() > 0)
                {
                    var attribute = minfo.GetCustomAttributes(true).Where(x => x.GetType().Name == "OperationContractAttribute").ToList();

                    if (attribute.Count > 0)
                    {
                        OperationContractAttribute = new OperationContractAttribute();

                        //avoid null exception in string convertion if value is null
                        OperationContractAttribute.NameAttribute = attribute.Select(x => (x.GetType().GetProperty("Name").GetValue(x, null) ?? string.Empty).ToString()).FirstOrDefault();

                        OperationContractAttribute.ActionAttribute = attribute.Select(x => (x.GetType().GetProperty("Action").GetValue(x, null) ?? string.Empty).ToString()).FirstOrDefault();

                        OperationContractAttribute.ReplyActionAttribute = attribute.Select(x => (x.GetType().GetProperty("ReplyAction").GetValue(x, null) ?? string.Empty).ToString()).FirstOrDefault();
                    }
                }
                return OperationContractAttribute;
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }
        private static ServiceContractAttribute GetServiceContractAttribute(Type type)
        {
            try
            {
                ServiceContractAttribute ServiceContractAttribute = null;

                string[] attributeTypes = type.GetCustomAttributes(true).Select(x => x.GetType().Name).ToArray();

                if (Array.FindAll(attributeTypes, a => a.Equals("ServiceContractAttribute")).Count() > 0)
                {
                    var attribute = type.GetCustomAttributes(true).Where(x => x.GetType().Name == "ServiceContractAttribute").ToList();

                    if (attribute.Count > 0)
                    {
                        ServiceContractAttribute = new ServiceContractAttribute();

                        //avoid null exception in string convertion if value is null
                        ServiceContractAttribute.NameAttribute = attribute.Select(x => (x.GetType().GetProperty("Name").GetValue(x, null) ?? string.Empty).ToString()).FirstOrDefault();

                        ServiceContractAttribute.NamespaceAttribute = attribute.Select(x => (x.GetType().GetProperty("Namespace").GetValue(x, null) ?? string.Empty).ToString()).FirstOrDefault();

                        ServiceContractAttribute.ConfigurationNameAttribute = attribute.Select(x => (x.GetType().GetProperty("ConfigurationName").GetValue(x, null) ?? string.Empty).ToString()).FirstOrDefault();
                    }
                }
                return ServiceContractAttribute;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}