﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WCFService.Reflection
{   
    /// <summary>
    /// "BasicHttpBinding", 
    ///"WSHttpBinding", 
    ///"WSDualHttpBinding", 
    ///"WSFederationHttpBinding
    ///"NetTcpBinding", 
    ///"NetNamedPipeBinding"
    ///"NetMsmqBinding",
    ///"NetPeerTcpBinding"
    ///"MsmqIntegrationBinding"
    /// </summary>
    public class DefaultBinding
    {
        public static string BasicHttpBinding { get { return "BasicHttpBinding"; } }

        public static readonly Dictionary<string, string[]> Bindings = new Dictionary<string, string[]>() {
             { "basichttpbinding", new[] { "http:", "https:" } },

             { "wshttpbinding", new[] { "http:", "https:" } },

             { "wsdualhttpbinding", new[] { "http:", "https:" } },

             { "wsfederationhttpbinding", new[] { "http:", "https:" } },

             { "nettcpbinding", new[] { "tcp" } },

             { "netnamedpipebinding", new[] { "pipe" } },

             { "netmsmqbinding", new[] { "msmq" } },

             { "netpeertcpbinding", new[] { "p2p" } },

             { "msmqintegrationbinding", new[] { "msmq" } }

        };
    }
}
