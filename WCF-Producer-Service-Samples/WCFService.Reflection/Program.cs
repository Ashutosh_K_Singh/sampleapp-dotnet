﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Web.Administration;
using System.Web.Hosting;

namespace WCFService.Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceModelDLLExtractor.LoadWCFAssembly(@"C:\Users\rajneesh.kumar\source\repos\WCFService\WCFService.ASPNETClient\bin\WCFService.ASPNETClient.dll");
            //   Uri uri = new Uri("net.tcp://www.contoso.com:8080/letters/readme.html");
            //try
            //{
            //    string directoryPath = @"C:\Users\rajneesh.kumar\source\repos\WCFService\WCFAddition_IIS\bin";

            //    var applicationDomain = SERVERINFO.GetDomainName();

            //    var url = SERVERINFO.SiteUrl("WCF Samples Site", "WCFAddition_IIS");

            //    var binding = SERVERINFO.ApplicationServerBinding("WCFAddition_IIS");

            //    GetApplicationInfo();

            //    var serviceProxies = WCFServiceProxy.ServiceProxy(directoryPath);

            //    serviceProxies.ForEach(f => { Console.WriteLine("Address={0}\n Binding={1}\n Contract={2}\n", f.Address, f.Binding, f.Contract); });

            //    Console.Read();
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }

        public static void GetApplicationInfo()
        {
            
            ServerManager mgr = new ServerManager();
            string SiteName = HostingEnvironment.ApplicationHost.GetSiteName();
            Site currentSite = mgr.Sites[SiteName];

            //The following obtains the application name and application object
            //The application alias is just the application name with the "/" in front

            string ApplicationAlias = HostingEnvironment.ApplicationVirtualPath;
            string ApplicationName = ApplicationAlias.Substring(1);
            Application app = currentSite.Applications[ApplicationAlias];
        }

    }
}