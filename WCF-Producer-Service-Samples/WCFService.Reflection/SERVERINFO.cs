﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.Administration;
namespace WCFService.Reflection
{
    public class SERVERINFO
    {
        public static Binding ApplicationServerBinding(string siteName, string protocol)
        {
            Binding binding = null;
            using (ServerManager serverManager = new ServerManager())
            {
                Site site = serverManager.Sites.FirstOrDefault(s => s.Name == siteName);
                if (site != null)
                {
                    binding = site.Bindings.Where(b => b.Protocol == protocol).FirstOrDefault();
                    Console.WriteLine(binding.BindingInformation.ToString());
                }
                return binding;
            }
        }

        public static BindingCollection ApplicationServerBinding(string siteName)
        {
            BindingCollection binding = null;
            using (ServerManager serverManager = new ServerManager())
            {
                Site site = serverManager.Sites.FirstOrDefault(s => s.Name == siteName);
                if (site != null)
                {
                    binding = site.Bindings;

                }
                return binding;
            }
        }

        public static List<string[]> SiteUrl(string siteName, string application)
        {
            Console.WriteLine("MachineName: {0},UserDomainName: {1}", Environment.MachineName, Environment.UserDomainName);
            ServerManager serverMgr = new ServerManager();
       
            //Site site = serverMgr.Sites[siteName];
            //foreach (Binding binding in site.Bindings)
            //{
            //    string bindingInfo = binding.BindingInformation;
            //    string subString = bindingInfo.Substring(2, bindingInfo.Length - 2);
            //    string[] adrs = subString.Split(':');
            //    adrs[0] = "localhost:" + adrs[0];
            //    urls.Add(adrs);
            //}


            foreach (var site in serverMgr.Sites)
            {
                Console.WriteLine("Site: {0}", site.Name);
                foreach (var app in site.Applications)
                {
                    Console.WriteLine(app.Path);
                }
            }
            List<string[]> urls = new List<string[]>();
            

            return urls;
        }

        public static string GetDomainName()
        {
            string fqdn = string.Empty;
            try
            {
                string domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
                string hostName = System.Net.Dns.GetHostName();

                if (!hostName.Contains(domainName))
                    fqdn = hostName + "." + domainName;
                else
                    fqdn = hostName;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return fqdn;
        }
    }
}
