﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace WCFService.Reflection
{
    public class SVCUtil
    {
        public static void RunSVCUtilToGenerateProxyFiles(string command)
        {

            ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/c " + command);
            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.UseShellExecute = false;
            procStartInfo.CreateNoWindow = true;

            // wrap IDisposable into using (in order to release hProcess) 
            using (Process process = new Process())
            {
                process.StartInfo = procStartInfo;
                process.Start();

                // Add this: wait until process does its work
                process.WaitForExit();

                // and only then read the result
                string result = process.StandardOutput.ReadToEnd();
                Console.WriteLine(result);
            }
        }
        /* 
         * --- SVC Markup Example--- 
          <%@ ServiceHost Language="C#" Debug="true" Service="WCFService.ClassLibrary.Calculater" Factory = "Factory, FactoryNamespace" CodeBehind = "CodeBehind"%>
          <%@ Assembly Src="WCFService.ClassLibrary.Calculater" %>
       */
        public static IDictionary<string, string> ReadSVCMarkup(string fileName)
        {
            IDictionary<string, string> dic = null;
            try
            {
                string line = null;

                string targetStart = "<%@";

                string targetEnd = "%>";

                using (var reader = new StreamReader(fileName))
                {
                    line = reader.ReadToEnd();

                    line = line.Replace(targetStart, string.Empty).Replace(" ", string.Empty);

                    var elementContent = SplitSVCTextByTag(line, targetEnd);

                    var attributes = new[] { "Service", "CodeBehind", "Factory", "Src" };

                    dic = GetElementAttributeValues(attributes, elementContent);

                    return dic;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }
        private static IDictionary<string, string> GetElementAttributeValues(string[] attributes, string[] elementContent)
        {

            try
            {
                IDictionary<string, string> dic = new Dictionary<string, string>();

                foreach (var attr in attributes)
                {
                    foreach (var elem in elementContent)
                    {
                        KeyValuePair<string, string> pair = GetSVCAttributes(elem, attr).Select(s => s.Split('=')).ToDictionary(d => d[0], d => d[1].Replace("\"", string.Empty)).FirstOrDefault();

                        if (!string.IsNullOrEmpty(pair.Key))
                        {
                            dic.Add(pair);
                        }
                    }
                }

                return dic;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static List<string> GetSVCAttributes(string content, string attributeName)
        {
            try
            {
                string pattern = "" + attributeName + "=\".*?\"";

                var matches = Regex.Matches(content, pattern, RegexOptions.IgnoreCase);

                List<string> matchelist = new List<string>();

                foreach (var item in matches)
                {
                    matchelist.Add(item.ToString());

                    Console.WriteLine("SVC markup contains attibute: {0}\n", item.ToString());
                }

                return matchelist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string[] SplitSVCTextByTag(string svcContent, string tag)
        {
            try
            {
                var trimfor = new char[] { '\r', '\n', '\t' };

                string[] splited_elm = svcContent.Split(new[] { tag }, StringSplitOptions.RemoveEmptyEntries);

                var trim_elm = splited_elm.Select(e => e.Trim(trimfor)).ToArray();

                //get none empty strings
                trim_elm = trim_elm.Where(te => te.Count() > 0).Select(et => et).ToArray();

                return trim_elm;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static bool IsAbsolutePatternMatch(string content)
        {
            try
            {
                string pattern = @"\w+:(\/?\/?)[^\s]+";
                return Regex.IsMatch(content, pattern, RegexOptions.IgnoreCase);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static bool MatchTransportBinding(string[] binding, string hostAdr)
        {
            try
            { //multiple protocol
                var pattern = binding.Aggregate((s1, s2) => s1 + "|" + s2);

                return Regex.IsMatch(hostAdr, pattern, RegexOptions.IgnoreCase);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static string[] GetFilesFromDirectory(string path, string fileExtention, SearchOption searchOption)
        {
            string file = string.Format("*.{0}", fileExtention);

            return Directory.GetFiles(path, file, searchOption);
        }
    }
}