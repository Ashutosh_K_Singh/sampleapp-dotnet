﻿

namespace WCFService.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /*DTO For WCF Configuration*/
    public class ConfigServiceModelDTO
    {
        public ConfigServiceModelDTO()
        {
            this.Endpoints = new List<Endpoint>();
        }
        public string Service { get; set; }
        public List<string> BaseAddressPrefix { get; set; }
        public List<string> HostBaseAddress { get; set; }
        public List<Endpoint> Endpoints { get; set; }
    }
    public class Endpoint
    {
        public string Address { get; set; }
        public string BindingName { get; set; }
        public string Contract { get; set; }
        public string ListenUri { get; set; }
    }

    /*DTO For Dll Types*/
    public class ServiceModelDTO : ServiceContractAttribute
    {
        public ServiceModelDTO()
        {
            this.TypeMethods = new List<Method>();
        }
        public string ClassName { get; set; }
        public Type Type { get; set; }
        public bool IsInterface { get; set; }
        public string TypeAssemblyName { get; set; }
        public string TypeFullName { get; set; }
        public string TypeName { get; set; }
        public string TypeNamespace { get; set; }
        public List<Method> TypeMethods { get; set; }
        public string ServiceFileName { get; set; }
    }
    public class Method : OperationContractAttribute
    {
        public string MethodName { get; set; }
        public string MethodFullName { get; set; }
        public List<string> MethodParmeters { get; set; }
        public string MethodReturnType { get; set; }
        public string MethodSignature { get; set; }
    }
    public class ServiceContractAttribute
    {
        public string ConfigurationNameAttribute { get; set; }
        public string NameAttribute { get; set; }
        public string NamespaceAttribute { get; set; }
    }
    public class OperationContractAttribute
    {
        public string NameAttribute { get; set; }
        public string ActionAttribute { get; set; }
        public string ReplyActionAttribute { get; set; }
        public bool IsOneWayAttribute { get; set; }
    }

    /*DTO for proxy generation*/
    public class ServiceProxy
    {
        public string Address { get; set; }
        public string Binding { get; set; }
        public string Contract { get; set; }
    }
    public class SVCDTO
    {
        public string SvcFileName { get; set; }
        public Dictionary<string, string> SVCMarkupAttribute { get; set; }
    }

    public class ApplicationFile
    {
        public string AppDirectory { get; set; }
        public string ConfigFile { get; set; }
        public string[] SVCFile { get; set; }
        public string[] DllFile { get; set; }

    }
}
