﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel.Configuration;

namespace WCFService.Reflection
{
    public class WCFConfigExtractor
    {
        public static List<ConfigServiceModelDTO> ExtractWCFConfiguration(string assemblyPath)
        {
            try
            {
                ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();

                configMap.ExeConfigFilename = assemblyPath;

                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);

                ServiceModelSectionGroup configurationSectionGroup = config.GetSectionGroup("system.serviceModel") as ServiceModelSectionGroup;

                //Services for defined services
                ServiceElementCollection services = configurationSectionGroup.Services.Services;

                List<ConfigServiceModelDTO> ServiceDTOs = GetServiceInfo(services, configurationSectionGroup.ServiceHostingEnvironment);

                return ServiceDTOs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static List<string> GetBaseAddressFromHostingEnvironment(ServiceHostingEnvironmentSection serviceHostingEnvironmentSection)
        {
            try
            {
                List<string> prefixList = new List<string>();

                foreach (BaseAddressPrefixFilterElement bpf in serviceHostingEnvironmentSection.BaseAddressPrefixFilters)
                {
                    prefixList.Add(bpf.Prefix.OriginalString);
                }
                return prefixList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static List<ConfigServiceModelDTO> GetServiceInfo(ServiceElementCollection services, ServiceHostingEnvironmentSection serviceHostingEnvironmentSection)
        {
            try
            {
                List<ConfigServiceModelDTO> dtoServiceList = new List<ConfigServiceModelDTO>();

                foreach (ServiceElement service in services)
                {
                    ConfigServiceModelDTO dtoService = new ConfigServiceModelDTO();

                    dtoService.Service = service.Name;

                    //BaseAddressPrefix information if exists in ServiceHostingEnvironment tag
                    dtoService.BaseAddressPrefix = GetBaseAddressFromHostingEnvironment(serviceHostingEnvironmentSection);

                    //hosting Base address information                
                    dtoService.HostBaseAddress = GetBaseAddressInfoFromHost(service.Host);

                    //Endpoint Information               
                    dtoService.Endpoints = GetServiceEndpointInfo(service.Endpoints);

                    dtoServiceList.Add(dtoService);
                }
                return dtoServiceList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static List<Endpoint> GetServiceEndpointInfo(ServiceEndpointElementCollection endpoints)
        {
            try
            {
                List<Endpoint> endPointList = new List<Endpoint>();

                foreach (ServiceEndpointElement ep in endpoints)
                {
                    Endpoint endpoint = new Endpoint();

                    endpoint.Address = ep.Address.OriginalString;

                    endpoint.BindingName = ep.Binding;

                    endpoint.Contract = ep.Contract;

                    endPointList.Add(endpoint);
                }
                return endPointList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static List<string> GetBaseAddressInfoFromHost(HostElement hostElement)
        {
            try
            {
                BaseAddressElementCollection hostBaseAddress = hostElement.BaseAddresses;

                List<string> hostUri = new List<string>();

                foreach (BaseAddressElement baseAddress in hostBaseAddress)
                {
                    hostUri.Add(baseAddress.BaseAddress);
                }
                return hostUri;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}