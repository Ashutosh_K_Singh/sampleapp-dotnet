﻿namespace WCFService.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public class WCFServiceProxy
    {
        public static List<ServiceProxy> ServiceProxy(string directoryPath)
        {
            try
            {              
                ApplicationFile appfiles = ApplicationDirectoryPath(directoryPath, out string appDirectory);

                List<SVCDTO> SVCDTOs = MapSVCFileToDTO(appfiles.SVCFile, appDirectory);

                //pass WCF configuration file to get service configuration dto
                var configServiceModelDTO = WCFConfigExtractor.ExtractWCFConfiguration(appfiles.ConfigFile);

                return ProcessSVCDTOForServiceProxy(configServiceModelDTO, SVCDTOs, appfiles.DllFile);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static ApplicationFile ApplicationDirectoryPath(string binDirectory, out string appDirectory)
        {
            try
            {
                ApplicationFile applicationFile = new ApplicationFile();

                applicationFile.AppDirectory = binDirectory;

                binDirectory = binDirectory.Trim('\\');

                binDirectory = string.Format("{0}\\", binDirectory);

                appDirectory = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(binDirectory));

                //string bin_directory = System.IO.Directory.GetDirectories(appDirectory, "bin", System.IO.SearchOption.TopDirectoryOnly).FirstOrDefault();

                applicationFile.SVCFile = SVCUtil.GetFilesFromDirectory(appDirectory, "svc", System.IO.SearchOption.AllDirectories);

                applicationFile.ConfigFile = SVCUtil.GetFilesFromDirectory(appDirectory, "config", System.IO.SearchOption.TopDirectoryOnly).FirstOrDefault();

                applicationFile.DllFile = SVCUtil.GetFilesFromDirectory(binDirectory, "dll", System.IO.SearchOption.TopDirectoryOnly);

                Console.WriteLine("Directory Path: {0}\n", appDirectory);

                return applicationFile;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static List<ServiceProxy> ProcessSVCDTOForServiceProxy(List<ConfigServiceModelDTO> configServiceModelDTOs, List<SVCDTO> SVCDTOs, string[] assemblyPath)
        {
            try
            {
                List<ServiceProxy> serviceProxies = new List<ServiceProxy>();

                List<ServiceModelDTO> serviceModelDTOs = null;

                foreach (SVCDTO svc in SVCDTOs)
                {
                    foreach (var markup in svc.SVCMarkupAttribute)
                    {
                        foreach (var asm in assemblyPath)
                        {
                            //pass dll in which service classes defined
                            var assembly = ServiceModelDLLExtractor.LoadWCFAssembly(asm);

                            Type clrType = assembly.GetType(markup.Value);

                            if (clrType == null)
                            {
                                clrType = assembly.GetExportedTypes().Where(t => t.FullName == markup.Value).FirstOrDefault();
                            }

                            //works for service attribute
                            if (clrType != null)
                            {
                                serviceModelDTOs = new List<ServiceModelDTO>();

                                var listServiceModelDTO2 = ServiceModelDLLExtractor.GetImplimentedInterfaceFromCLRType(clrType, svc.SvcFileName, serviceModelDTOs);

                                var result = MapProxyDTOs(configServiceModelDTOs, listServiceModelDTO2);

                                serviceProxies.AddRange(result);
                            }
                        }
                    }
                }

                return serviceProxies;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static List<SVCDTO> MapSVCFileToDTO(string[] svcFiles, string appDirectory)
        {
            try
            {
                List<SVCDTO> SVCDTOs = new List<SVCDTO>();

                foreach (string svc in svcFiles)
                {
                    SVCDTO svcdto = new SVCDTO();

                    //pass service file reference to get service information
                    svcdto.SVCMarkupAttribute = SVCUtil.ReadSVCMarkup(svc).ToDictionary(x => x.Key, x => x.Value);

                    string svcRelateivePath = svc.Replace(appDirectory, string.Empty);
                    ////svcdto.SvcFileName = System.IO.Path.GetFileName(svc);

                    svcdto.SvcFileName = svcRelateivePath.TrimStart('\\').Replace('\\', '/');

                    SVCDTOs.Add(svcdto);
                }

                return SVCDTOs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string[] GetFilesFromDirectory(string path, string fileNameWithoutExtension, string fileExtension)
        {
            string file = string.Format("{0}.{1}", fileNameWithoutExtension, fileExtension);

            return System.IO.Directory.GetFiles(path, file);
        }
        private static List<ServiceProxy> MapProxyDTOs(List<ConfigServiceModelDTO> listConfigServiceModelDTO, List<ServiceModelDTO> listServicemodelDTO)
        {
            try
            {
                List<ServiceProxy> proxyList = new List<ServiceProxy>();

                //lookup for contract in both DTOs
                foreach (ServiceModelDTO smdto in listServicemodelDTO)
                {
                    if (smdto.IsInterface)
                    {
                        Console.WriteLine("ServiceModelDTO contains Contract: {0}\n", smdto.TypeFullName);

                        if (listConfigServiceModelDTO.Count > 0)
                        {
                            #region ## service configuration available
                            foreach (ConfigServiceModelDTO csmdto in listConfigServiceModelDTO)
                            {
                                Endpoint ep = csmdto.Endpoints.Where(e => e.Contract == smdto.TypeFullName).FirstOrDefault();

                                ServiceProxy proxy = null;

                                if (ep != null)
                                {
                                    proxy = new ServiceProxy();

                                    if (!string.IsNullOrEmpty(ep.Address))
                                    {
                                        //if address is an absolute address
                                        if (SVCUtil.IsAbsolutePatternMatch(ep.Address))
                                        {
                                            proxy.Address = ep.Address;
                                        }
                                        else
                                        {
                                            proxy.Address = BuildAddress(csmdto, smdto.ServiceFileName, ep.BindingName, ep.Address);
                                        }
                                    }
                                    else
                                    {
                                        proxy.Address = BuildAddress(csmdto, smdto.ServiceFileName, ep.BindingName, ep.Address);
                                    }

                                    proxy.Binding = ep.BindingName;

                                    proxy.Contract = ep.Contract;

                                    proxyList.Add(proxy);
                                }
                                else
                                {
                                    proxyList = BuildServiceContractAttributeProxy(smdto, csmdto);
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region ## Default proxy when service configuration not defined
                            proxyList = BuildServiceContractAttributeProxy(smdto, null);
                            #endregion
                        }
                    }
                }

                return proxyList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static string BuildAddress(ConfigServiceModelDTO csmdto, string serviceFileName, string endpointBindingName, string enpointAddress)
        {
            try
            {
                string epBindingName = endpointBindingName.ToLower();

                string epAddress = enpointAddress.Trim('/').ToLower();

                string absoulteUrl = string.Empty;

                var bindingTransport = DefaultBinding.Bindings.Single(b => b.Key == epBindingName);

                if (csmdto.BaseAddressPrefix.Count > 0)
                {
                    absoulteUrl = BuildServiceAbsoluteUrl(csmdto.BaseAddressPrefix, serviceFileName, epAddress, bindingTransport.Value);
                }
                else if (csmdto.HostBaseAddress.Count > 0)
                {
                    absoulteUrl = BuildServiceAbsoluteUrl(csmdto.HostBaseAddress, serviceFileName, epAddress, bindingTransport.Value);
                }

                return absoulteUrl;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static string BuildServiceAbsoluteUrl(List<string> baseAddress, string serviceFileName, string epAddress, string[] bindingTransportValue)
        {
            try
            {
                string absoulteUrl = string.Empty;

                foreach (string address in baseAddress)
                {
                    //Check Address for specified binding for this endpoint

                    if (SVCUtil.MatchTransportBinding(bindingTransportValue, address))
                    {
                        absoulteUrl = string.Format("{0}/{1}/{2}", address, serviceFileName, epAddress);
                    }
                }

                return absoulteUrl.TrimEnd('/').ToLower();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static List<ServiceProxy> BuildServiceContractAttributeProxy(ServiceModelDTO smdto, ConfigServiceModelDTO csmdto)
        {
            try
            {
                var binding = SERVERINFO.ApplicationServerBinding("WCFMultipleBindingSample", "net.tcp");

                List<ServiceProxy> proxies = new List<ServiceProxy>();

                if (csmdto == null)
                {
                    ServiceProxy proxy = new ServiceProxy();

                    proxy.Address = string.Format("{0}://localhost:{1}/{2}", binding.Protocol, binding.EndPoint.Port, smdto.ServiceFileName).TrimEnd('/').ToLower();

                    proxy.Binding = DefaultBinding.BasicHttpBinding;

                    proxy.Contract = String.IsNullOrEmpty(smdto.NameAttribute) ? smdto.TypeFullName : smdto.NameAttribute;

                    proxies.Add(proxy);
                }
                else if (csmdto.BaseAddressPrefix.Count > 0)
                {
                    proxies = BuildProxyDTO(csmdto.BaseAddressPrefix, smdto);
                }
                else if (csmdto.HostBaseAddress.Count > 0)
                {
                    proxies = BuildProxyDTO(csmdto.HostBaseAddress, smdto);
                }

                return proxies;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static List<ServiceProxy> BuildProxyDTO(List<string> baseAdress, ServiceModelDTO smdto)
        {
            try
            {
                string absoulteUrl = string.Empty;

                List<ServiceProxy> listProxy = new List<ServiceProxy>();

                foreach (string hostAdd in baseAdress)
                {
                    ServiceProxy proxy = new ServiceProxy();

                    absoulteUrl = string.Format("{0}/{1}", hostAdd, smdto.ServiceFileName);

                    proxy.Address = absoulteUrl.TrimEnd('/').ToLower();

                    proxy.Binding = DefaultBinding.BasicHttpBinding;

                    proxy.Contract = String.IsNullOrEmpty(smdto.NameAttribute) ? smdto.TypeFullName : smdto.NameAttribute;

                    listProxy.Add(proxy);
                }
                return listProxy;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
