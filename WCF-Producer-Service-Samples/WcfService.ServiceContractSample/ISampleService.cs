﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService.ServiceContractSample
{
    [ServiceContract(
    Namespace = "http://microsoft.wcf.documentation",
    Name = "proxySampleService",
    ProtectionLevel = ProtectionLevel.EncryptAndSign
      
  )]
    public interface ISampleService
    {
        [OperationContract]
        string SampleMethod(string msg);
    }
}
