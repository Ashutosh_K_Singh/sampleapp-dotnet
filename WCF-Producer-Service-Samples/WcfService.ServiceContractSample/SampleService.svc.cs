﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService.ServiceContractSample
{
    class SampleService : ISampleService
    {
        #region ISampleService Members

        public string SampleMethod(string msg)
        {
            return "The service greets you: " + msg;
        }

        #endregion
    }
}
