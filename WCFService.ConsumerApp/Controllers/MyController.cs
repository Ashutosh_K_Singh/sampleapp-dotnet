﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WCFService.ConsumerApp.SeparatAssemblySample;
using WCFService.ConsumerApp.SeprateAssembly_Northwind_CategoryServiceHost;
using WCFService.ConsumerApp.SeprateAssembly_Northwind_ProductServiceHost;
using WCFService.ConsumerApp.ServiceContractSample;
using WCFService.ConsumerApp.WCFAddition_IIS;
using WCFService.ConsumerApp.WCFMultipleBindingSample;
using WCFService.ConsumerApp.WcfServiceDefaultConfig;

namespace WCFService.ConsumerApp.Controllers
{
    public class MyController : Controller
    {
        Service1Client service1_client = new Service1Client();
        CategoryServiceClient categoryservice_client = new CategoryServiceClient();
        ProductServiceClient productservice_client = new ProductServiceClient();
        //proxySampleServiceClient proxysampleservice_client = new proxySampleServiceClient();
        WCFAdditionClient wcfaddition_iis_client = new WCFAdditionClient();
        CalculaterClient multiplebindingsamplecalculater_client = new CalculaterClient();
        DefaultClient default_client = new DefaultClient();

        // GET: My
        public ActionResult Index()
        {
            default_client.DoWork();

            ViewBag.categoryservice_client_GetCategoryName = categoryservice_client.GetCategoryName(1);

            ViewBag.productservice_client_GetProductName = productservice_client.GetProductName(1);

            //ViewBag.proxysampleservice_client_SampleMethod = proxysampleservice_client.SampleMethod("Hello");

            ViewBag.wcfaddition_iis_client_SumOfTwoNumber = wcfaddition_iis_client.SumOfTwoNumber(2, 10);

            ViewBag.multiplebindingsamplecalculater_client_Multiply = multiplebindingsamplecalculater_client.Multiply(2, 10);

            return View();
        }

        // GET: My/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
    }
}
